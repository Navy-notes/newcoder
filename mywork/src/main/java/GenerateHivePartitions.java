import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;

/**
 * @author Yuliang.Lee
 * @version 1.0
 * @date 2022/12/26 09:22
 */

public class GenerateHivePartitions {
    static String start = "20200601"; //20200601
    static String end = "20221225";

    public static void main(String[] args) {
        DateTime startTime = DateUtil.parse(start, "yyyyMMdd");
        DateTime endTime = DateUtil.parse(end, "yyyyMMdd");
        DateTime cur = startTime;

        StringBuilder sb = new StringBuilder(1000);
        int num = 0;
        while(DateUtil.compare(cur, endTime) != 0) {
            // 清算日期yyyyMMdd
            String curFilePath = DateUtil.format(cur, "yyyyMMdd");
            // 实际出行日期yyyy-MM-dd = 清算日期-1
            String curPartition = DateUtil.format(DateUtil.offsetDay(cur, -1), "yyyy-MM-dd");
            // 生成分区语句
//            String addPatitionSql = "ALTER TABLE lnt_card_trade3 ADD IF NOT EXISTS PARTITION (pdate='" + curPartition + "') " +
//                    "LOCATION 'hdfs://standby/production/trade_liq/lnt_card_trade/" + curFilePath + "';";
            String addPatitionSql = "ALTER TABLE ykt_card_trade3 ADD IF NOT EXISTS PARTITION (pdate='" + curPartition + "') " +
                    "LOCATION 'hdfs://standby/production/trade_liq/ykt_card_trade_new/" + curFilePath + "';";
            sb.append(addPatitionSql);
            num++;
            cur = DateUtil.offsetDay(cur, 1);
        }

        System.out.println("构造分区 " + num + " 个");
        System.out.println(sb);
    }
}
