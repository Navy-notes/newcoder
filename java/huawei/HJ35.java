package huawei;

import java.util.Scanner;

/**
 * @author Yuliang.Lee
 * @version 1.0
 * @date 2021/9/15 11:52
 * 蛇形矩阵:
    蛇形矩阵是由1开始的自然数依次排列成的一个矩阵上三角形。
    例如，当输入5时，应该输出的三角形为：
    1 3 6 10 15
    2 5 9 14
    4 8 13
    7 12
    11
 */
public class HJ35 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNext()) {
            int n = in.nextInt();
            // 初始化
            int head = 1;
            // 迭代计算并输出
            for (int row = 0; row < n; row++) {
                head += row;
                int colElement = head;
                System.out.print(colElement + " ");
                for (int col = 2; col <= n - row; col++) {
                    colElement += (row + col);
                    System.out.print(colElement + " ");
                }
                System.out.println();
            }
        }
    }
}
