package huawei;

import java.util.Scanner;

/**
 * @author Yuliang.Lee
 * @version 1.0
 * @date 2021/9/4 17:06
 * 字符串排序：（选择排序和冒泡排序变式）
   编写一个程序，将输入字符串中的字符按如下规则排序。
    规则 1 ：英文字母从 A 到 Z 排列，不区分大小写。
    如，输入： Type 输出： epTy
    规则 2 ：同一个英文字母的大小写同时存在时，按照输入顺序排列。
    如，输入： BabA 输出： aABb
    规则 3 ：非英文字母的其它字符保持原来的位置。
    如，输入： By?e 输出： Be?y
 */
public class HJ26 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNext()) {
            String input = in.nextLine();
            char[] arr = input.toCharArray();
            char tmp;
            // 选择排序算法是不稳定的，会破坏同一个大小写字母的出现次序，这里采用冒泡排序
            for (int i = arr.length - 1; i > 0; i--) {
                for (int j = 0; j < i; j++) {
                    if (isEnglishLetter(arr[j])) {
                        //需要跳过非英文字母的
                        for (int k = 1; j + k <= i; k++) {
                            if (isEnglishLetter(arr[j+k])) {
                                // 统一转换成小写字母来处理
                                int left = arr[j] < 'a' ? arr[j] + 32 : arr[j];
                                int right = arr[j+k] < 'a' ? arr[j+k] + 32 : arr[j+k];
                                if (left > right) {
                                    tmp = arr[j];
                                    arr[j] = arr[j+k];
                                    arr[j+k] = tmp;
                                }
                                break;
                            }
                        }
                    }
                }
            }
            for (char c : arr) {
                System.out.print(c);
            }
            System.out.println();
        }
    }

    public static boolean isEnglishLetter(char ch) {
        if ((ch >= 'A' && ch <= 'Z') || (ch >= 'a' && ch <= 'z')) {
            return true;
        }
        return false;
    }
}
