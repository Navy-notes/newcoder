package huawei;

import java.util.Scanner;

/**
 * @author Yuliang.Lee
 * @version 1.0
 * @date 2021/8/19 11:14
 * 取近似值
 * Math.round四舍五入的实现其实是：不管是浮点数正或负，一律+0.5后向下取整
 */
public class HJ7 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNextFloat()) {
            float n = in.nextFloat();
            System.out.println(Math.round(n));
        }
    }
}
