package huawei;

import java.util.Arrays;
import java.util.Scanner;

/**
 * @author Yuliang.Lee
 * @version 1.0
 * @date 2021/9/2 10:03
 * 合唱队（最长递增子序列的动态规划问题）：
    计算最少出列多少位同学，使得剩下的同学排成合唱队形。
   说明：
    N位同学站成一排，音乐老师要请其中的(N-K)位同学出列，使得剩下的K位同学排成合唱队形。
    合唱队形是指这样的一种队形：设K位同学从左到右依次编号为1，2…，K，他们的身高分别为T1，T2，…，TK，则他们的身高满足存在i（1<=i<=K）使得T1<T2<......<Ti-1<Ti>Ti+1>......>TK。
    你的任务是，已知所有N位同学的身高，计算最少需要几位同学出列，可以使得剩下的同学排成合唱队形。
   注意：
    不允许改变队列元素的先后顺序且不要求最高同学左右人数必须相等。
   解题思路：
    由于队列不改变先后顺序，这里可以类比成最长递增子序列问题，并建立相应的动态规划模型。
    先求出每个人左边（包括自己）按照身高递增最多能排多少人；然后再求每个人右边（包括自己）按照身高递减最多能排多少人。
    然后两次结果的列表对应的下标相加，并取最大的和再减1（因为每个人自身都加了两次）即是合唱队型的最大人数。
    最少出列人数 = 总人数 - 合唱队形最大人数
 * 示例：
    输入
    8
    186 186 150 200 160 130 197 200
    输出
    4

 * 最长递增子序列问题的描述：
    设L=<a1,a2,…,an>是n个不同的实数的序列，L的递增子序列是这样一个子序列Lin=<aK1,ak2,…,akm>，
    其中k1<k2<…<km且aK1<ak2<…<akm。求最大的m值。
 * 动态规划解法：
    设f(i)表示序列L中以ai为末元素的最长递增子序列的长度（1 <= i <= n）
    显然，f(0) = 0
         f(1) = 1
    f(i)的迭代关系：
        f(i) = 1
        for (j=i-1; j>0; j--){
            if (ai > aj && f(i) < f(j)+1){
                f(i) = f(j)+1
            }
        }
 */
public class HJ24 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNext()) {
            int n = in.nextInt();
            int[] queue = new int[n+1];
            for (int i = 1; i <= n; i++) {
                queue[i] = in.nextInt();
            }

            // 第n位同学（含）左边递增的最大排列数
            int[] f1 = new int[n+1];
            // 第n位同学（含）右边递减的最大排列数
            int[] f2 = new int[n+1];
            // 初始化
            f1[1] = 1;
            f2[n] = 1;

            // 动态规划计算左递增
            for (int i = 2; i <= n; i++) {
                f1[i] = 1;
                for (int j = i-1; j >= 1; j--) {
                    if (queue[i] > queue[j] && f1[i] < f1[j]+1) {
                        f1[i] = f1[j] + 1;
                    }
                }
            }
            // 动态规划计算右递减
            for (int i = n-1; i >= 1; i--) {
                f2[i] = 1;
                for (int j = i+1; j <= n ; j++) {
                    if (queue[i] > queue[j] && f2[i] < f2[j]+1) {
                        f2[i] = f2[j] + 1;
                    }
                }
            }

            // 合唱队列的最大人数
            int maxNum = 0;
            for (int i = 1; i <= n; i++) {
                int currentNum = f1[i] + f2[i] - 1;
                maxNum = currentNum > maxNum ? currentNum : maxNum;
            }

//            for (int i : f1) {
//                System.out.print(i + " ");
//            }
//            System.out.println();
//            for (int i : f2) {
//                System.out.print(i + " ");
//            }
//            System.out.println();
            System.out.println(n - maxNum);
        }
    }
}
