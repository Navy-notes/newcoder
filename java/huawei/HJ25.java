package huawei;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

/**
 * @author Yuliang.Lee
 * @version 1.0
 * @date 2021/9/3 11:41
 * 数据分类处理：
    https://www.nowcoder.com/practice/9a763ed59c7243bd8ab706b2da52b7fd?tpId=37&&tqId=21248&rp=1&ru=/ta/huawei&qru=/ta/huawei/question-ranking

 * 数据结构定义：
    输入： 一个TreeSet<Integer>、String[]
    输出：一个结果List<Object>、一个临时List<Object>
 */
public class HJ25 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNext()) {
            // 初始化输入数据
            int numL = in.nextInt();
            String[] arrayL = new String[numL];
            for (int i = 0; i < numL; i++) {
                arrayL[i] = in.next();
            }
            int numR = in.nextInt();
            Set<Integer> setR = new TreeSet<>();
            for (int i = 0; i < numR; i++) {
                setR.add(in.nextInt());
            }

            // 定义输出数据结构
            List<Object> tmpList = new ArrayList<>();
            List<Object> resultList = new ArrayList<>();

            // 数据分类处理
            for (Integer e : setR) {
                String str = String.valueOf(e);
                for (int i = 0; i < numL; i++) {
                    if (arrayL[i].contains(str)) {
                        tmpList.add(i);
                        tmpList.add(arrayL[i]);
                    }
                }
                if (tmpList.size() > 0) {
                    resultList.add(e);
                    resultList.add(tmpList.size() / 2);
                    resultList.addAll(tmpList);
                    tmpList.clear();
                }
            }

            // 结果输出
            System.out.print(resultList.size() + " ");
            for (Object o : resultList) {
                System.out.print(o + " ");
            }
            System.out.println();
        }
    }
}
