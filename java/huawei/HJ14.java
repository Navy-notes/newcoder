package huawei;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

/**
 * @author Yuliang.Lee
 * @version 1.0
 * @date 2021/8/23 10:43
 * 字符串排序：数据输出n行，输出结果为按照字典序排列的字符串。
 */
public class HJ14 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()) {
            int n = scanner.nextInt();
            List<String> list = new ArrayList<>();
            for (int i = 0; i < n; i++) {
                list.add(scanner.next());
            }
            /**
             * 使用list排序有两种方式。
             * 方式一： 泛型的POJO类中实现Comparable接口，然后调用Collections.sort(list)方法即可
             * 方式二： 调用实例list.sort(Comparator<? super E> c)，需要额外单独实现Comparator接口
             *
             * 对于String类，默认使用字典序递增排序。
             * 规则：数字排在字母前面，大写字母在小写字母前面，字母之间按照abc-xyz的顺序排序。
             */
            Collections.sort(list);
            for (String s : list) {
                System.out.println(s);
            }
        }
    }
}
