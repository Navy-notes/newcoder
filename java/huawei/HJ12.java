package huawei;

import java.util.Scanner;

/**
 * @author Yuliang.Lee
 * @version 1.0
 * @date 2021/8/23 10:25
 * 字符串反转
 */
public class HJ12 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNextLine()) {
            StringBuilder input = new StringBuilder(in.nextLine());
            System.out.println(input.reverse().toString());
        }
    }
}
