package huawei;

import java.util.Arrays;
import java.util.Scanner;

/**
 * @author Yuliang.Lee
 * @version 1.0
 * @date 2021/9/14 10:41
 * 图片整理：
    Lily上课时使用字母数字图片教小朋友们学习英语单词，每次都需要把这些图片按照大小（ASCII码值从小到大）排列收好。请大家给Lily帮忙，通过C语言解决。
    Lily使用的图片包括"A"到"Z"、"a"到"z"、"0"到"9"。输入字母或数字个数不超过1024。
    Lily的所有图片按照从小到大的顺序输出。
 * 示例：
    输入：Ihave1nose2hands10fingers
    输出：0112Iaadeeefghhinnnorsssv
 */
public class HJ34 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNext()) {
            char[] images = in.nextLine().toCharArray();
            Arrays.sort(images);
            for (char image : images) {
                System.out.print(image);
            }
            System.out.println();
        }
    }
}
