package huawei;

import java.util.Scanner;

/**
 * @author Yuliang.Lee
 * @version 1.0
 * @date 2021/8/24 10:26
 * 位运算：求int型数据在内存中存储时1的个数
 */
public class HJ15 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNextInt()) {
            int input = in.nextInt();
            String bs = Integer.toBinaryString(input);  // 转二进制字符串
            int count = 0;                             //统计1的个数
            for (int i = 0; i < bs.length(); i++) {
                if ('1' == bs.charAt(i)) {
                    count++;
                }
            }
            System.out.println(count);
        }
    }
}
