package huawei;

import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

/**
 * @author Yuliang.Lee
 * @version 1.0
 * @date 2021/9/29 10:46
 * 输入一个单向链表，输出该链表中倒数第k个结点，链表的倒数第1个结点为链表的尾指针。
    链表结点定义如下：
        struct ListNode
        {
        int       m_nKey;
        ListNode* m_pNext;
        };
    正常返回倒数第k个结点指针，异常返回空指针。

 * 示例：
    输入：
    8
    1 2 3 4 5 6 7 8
    4
    输出：
    5
 */
public class HJ51 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNext()) {
            int n = in.nextInt();
            List<Integer> chain = new LinkedList<>();
            for (int i = 0; i < n; i++) {
                chain.add(in.nextInt());
            }
            int k = in.nextInt();
            if (k >= 1 && k <= n) {
                System.out.println(chain.get(n - k));
            } else {
                System.out.println(0);
            }
        }
    }
}
