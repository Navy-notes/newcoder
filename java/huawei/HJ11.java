package huawei;

import java.util.Scanner;

/**
 * @author Yuliang.Lee
 * @version 1.0
 * @date 2021/8/23 10:14
 * 数字颠倒：将这个整数以字符串的形式逆序输出
 */
public class HJ11 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNextLine()) {
//            StringBuilder input = new StringBuilder(in.nextLine());
//            System.out.println(input.reverse().toString());
            String input = in.nextLine();
            int i = input.length() - 1;
            while (i >= 0) {
                System.out.print(input.charAt(i));
                i--;
            }
        }
    }
}
