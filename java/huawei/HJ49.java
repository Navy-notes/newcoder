package huawei;

import java.util.Scanner;

/**
 * @author Yuliang.Lee
 * @version 1.0
 * @date 2021/9/22 20:21
 * 最大匹配子串：
    选定串1作为匹配串，串2作为被匹配串，设f(i,j)表示串1的子串(i...j)是否能在串2中找到相同的匹配串，0表示false，1表示true
    i,j 从0 → len-1遍历，如果父串匹配，那么子串也一定匹配。即：f(i, j)=1 → f(i+1, i+1~j)=1；判断f(i,j)是否为1，就看f(i-1,j)是否为1

 * 示例：
    输入：
    ixotitbgzoknoghrrjjwageedzjaghsjvqeqptfphqkvdtevqjjxqgdwfqunvxpxthzzjeprbcvmyphduapyoargbqmizivhfryqdonlritniosmdvuiyhdoykojzvauymskrzrncufasqzuerdehpqceclpbjtvbebuullzaqtokgkfzznumkvvdgwvuiacetbcbmbcrdoemcfjbuw
    nqbgetlzozptuajfmvpzqqkuuxrwlhjcuyohpcnzpgkgmieuuavdetngrrxlxfauugyccklhjbsdqriznggbgvmcbamzdtdffpujnxhosgvbllnyoqpvuamsylyxtpqhcbfxhrdznwswmcxhvmrhttipapeshhdzjpmkucwqtztfsyxzwupmdgmlcatlqqwglekqmrojldjfjmthmiriyyavtznlosbixdtjxsjtzfyvvvwsqqfpmxbnkzwotuelfqjlctoaobu
    ixfkieaaocalmxhfifyadnouljtezrnpnfoenespcaenyvzcjtppsaxegmeytqrkvdwugvouskcnnqnmhepquncvyvgkansquaotkgvlvplktrabaikeuubfupunpztpvvzdqaqgfmtzxlcxsipltzwjegpqjzclclvjsmqbmiyzvcujpvhupyhvhhjq
    ganxioafstgdpceecubqrngumbpjvwxdpzmragsunvfnmejbcvsoydtbbewybygpsmmyjuvezn
    输出：
    158 dzj
    71 ez
 */
public class HJ49 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNext()) {
            // 获取输入
            String str1 = in.nextLine();
            String str2 = in.nextLine();
            // 设f(i,j)表示串1的子串(i...j)是否能在串2中找到相同的匹配串。
            int size = str1.length();
            // 所有的元素默认值为false
            boolean[][] f = new boolean[size][size];
            // 记录最大匹配串的下标和长度
            int sIndex_1 = -1;
            int subLen = 0;
            // 搜寻匹配串
            for (int i = 0; i < size; i++) {
                for (int j = i; j < size; j++) {
                    // 迭代计算
                    if (i > 0 && f[i-1][j]) {
                        f[i][j] = true;
                    } else {
                        if (j > i && !f[i][j-1]) {
                            continue;
                        }
                        f[i][j] = str2.contains(str1.substring(i, j+1));
                    }
                    // 更新最大匹配串的信息
                    if (f[i][j] && j-i+1 > subLen) {
                        subLen = j-i+1;
                        sIndex_1 = i;
                    }
                }
            }
            // 最大匹配串
            String subPattern = str1.substring(sIndex_1, sIndex_1 + subLen);
            // 找到最大匹配串在str2中的位置
            int sIndex_2 = str2.indexOf(subPattern);
            // 输出结果
            System.out.println(sIndex_2 + " " + subPattern);
        }
    }
}
