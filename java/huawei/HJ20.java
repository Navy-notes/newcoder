package huawei;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Yuliang.Lee
 * @version 1.0
 * @date 2021/8/29 9:46
 * 校验密码有效性：
    密码要求：
    1.长度超过8位
    2.包括大小写字母.数字.其它符号,以上四种至少三种
    3.不能有相同长度大于2的子串重复

    示例1
    输入：
    021Abc9000
    021Abc9Abc1
    021ABC9000
    021$bc9000
    输出：
    OK
    NG
    NG
    OK
 */
public class HJ20 {
    public static String REGEX_1 = "[A-Z]";
    public static String REGEX_2 = "[a-z]";
    public static String REGEX_3 = "\\d";
    public static String REGEX_4 = "[^A-Za-z0-9]";

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNext()) {
            String password = in.nextLine();
            if (password.length() <= 8) {
                System.out.println("NG");
                continue;
            }

            int countRegex = 0;
            if (hasRegex(REGEX_1, password)) {
                countRegex++;
            }
            if (hasRegex(REGEX_2, password)) {
                countRegex++;
            }
            if (hasRegex(REGEX_3, password)) {
                countRegex++;
            }
            if (hasRegex(REGEX_4, password)) {
                countRegex++;
            }
            if (countRegex < 3) {
                System.out.println("NG");
                continue;
            }

            if (isSubRepeat(password)) {
                System.out.println("NG");
                continue;
            }

            System.out.println("OK");
        }
    }

    public static boolean hasRegex(String regex, String input) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(input);
        if (matcher.find()) {
            return true;
        }
        return false;
    }

    /**
     * 检查是否存在长度为3的多个相同的子串
     * @param input
     * @return
     */
    public static boolean isSubRepeat(String input) {
        for (int i = 0; i < input.length() - 3; i++) {
            // 如果用模式regex相关的话还需要处理转译字符
            if (input.substring(i + 1).contains(input.substring(i, i + 3))) {
                return true;
            }
        }
        return false;
    }
}
