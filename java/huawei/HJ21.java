package huawei;

import java.util.Scanner;

/**
 * @author Yuliang.Lee
 * @version 1.0
 * @date 2021/8/30 11:42
 * 简单密码密文
    变换规则：
        小写字母：abc--2, def--3, ghi--4, jkl--5, mno--6, pqrs--7, tuv--8 wxyz--9
        大写字母：变成小写之后往后移一位，如X，先变成小写，再往后移一位即y；如Z,则是变换成a
        密码中不存在空格，数字和其他符号都不做变换
    示例1
    输入：YUANzhi1987
    输出：zvbo9441987
    提示：'A' = 65，'a' = 97
 */
public class HJ21 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNext()) {
            String input = in.nextLine();
            char[] password = input.toCharArray();
            for (int i = 0; i < password.length; i++) {
                char ch = password[i];

                // 小写字母转译
                if (ch == 'a' || ch == 'b' || ch == 'c') {
                    password[i] = '2';
                    continue;
                }
                if (ch == 'd' || ch == 'e' || ch == 'f') {
                    password[i] = '3';
                    continue;
                }
                if (ch == 'g' || ch == 'h' || ch == 'i') {
                    password[i] = '4';
                    continue;
                }
                if (ch == 'j' || ch == 'k' || ch == 'l') {
                    password[i] = '5';
                    continue;
                }
                if (ch == 'm' || ch == 'n' || ch == 'o') {
                    password[i] = '6';
                    continue;
                }
                if (ch == 'p' || ch == 'q' || ch == 'r' || ch == 's') {
                    password[i] = '7';
                    continue;
                }
                if (ch == 't' || ch == 'u' || ch == 'v') {
                    password[i] = '8';
                    continue;
                }
                if (ch == 'w' || ch == 'x' || ch == 'y' || ch == 'z') {
                    password[i] = '9';
                    continue;
                }

                // 大写字母转译
                if ( ch >= 'A' && ch < 'Z') {
                    password[i] = (char) (ch + 32 + 1);
                    continue;
                }
                if (ch == 'Z') {
                    password[i] = 'a';
                }
            }

            System.out.println(String.valueOf(password));    // 用pswdArr.toString是返回的数组地址
        }
    }
}
