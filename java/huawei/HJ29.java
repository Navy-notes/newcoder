package huawei;

import java.util.Scanner;

/**
 * @author Yuliang.Lee
 * @version 1.0
 * @date 2021/9/6 12:08
 * 字符串加解密：
    1、对输入的字符串进行加解密，并输出。
        第一行输入是要加密的密码，第二行输入是加过密的密码
    2、加密方法为：
        当内容是英文字母时则用该英文字母的后一个字母替换，同时字母变换大小写,如字母a时则替换为B；字母Z时则替换为a；
        当内容是数字时则把该数字加1，如0替换1，1替换2，9替换0；
        其他字符不做变化。
    3、解密方法为加密的逆过程。
 * 示例：
   输入
    abcdefg
    BCDEFGH
   输出
    BCDEFGH
    abcdefg
 */
public class HJ29 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNext()) {
            String plaintext = in.nextLine();
            String ciphertext = in.nextLine();
            for (char e : encrypt(plaintext)) {
                System.out.print(e);
            }
            System.out.println();
            for (char d : decrypt(ciphertext)) {
                System.out.print(d);
            }
            System.out.println();
        }
    }

    public static char[] encrypt(String input) {
        char[] encryption = new char[input.length()];
        for (int i = 0; i < input.length(); i++) {
            char ch = input.charAt(i);
            if (ch >= 'A' && ch <= 'Z') {
                if (ch == 'Z') {
                    ch = 'a';
                }
                else {
                    ch = (char) (ch + 33);
                }
            } else if (ch >= 'a' && ch <= 'z') {
                if (ch == 'z') {
                    ch = 'A';
                }
                else {
                    ch = (char) (ch - 31);
                }
            } else if (ch >= '0' && ch <= '9') {
                if (ch == '9') {
                    ch = '0';
                }
                else {
                    ch = (char) (ch+1);
                }
            }
            encryption[i] = ch;
        }
        return encryption;
    }

    public static char[] decrypt(String input) {
        char[] decryption = new char[input.length()];
        for (int i = 0; i < input.length(); i++) {
            char ch = input.charAt(i);
            if (ch >= 'A' && ch <= 'Z') {
                if (ch == 'A') {
                    ch = 'z';
                }
                else {
                    ch = (char) (ch + 31);
                }
            } else if (ch >= 'a' && ch <= 'z') {
                if (ch == 'a') {
                    ch = 'Z';
                }
                else {
                    ch = (char) (ch - 33);
                }
            } else if (ch >= '0' && ch <= '9') {
                if (ch == '0') {
                    ch = '9';
                }
                else {
                    ch = (char) (ch-1);
                }
            }
            decryption[i] = ch;
        }
        return decryption;
    }
}
