package huawei;

import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

/**
 * @author Yuliang.Lee
 * @version 1.0
 * @date 2021/9/28 18:14
 * 单向链表删除指定值的结点：
    输入一个单向链表和一个节点的值，从单向链表中删除等于该值的节点，删除后如果链表中无节点则返回空指针。
    （链表的值不会重复，且测试用例保证输入合法）

 * 示例1：
    输入：6 2 1 2 3 2 5 1 4 5 7 2 2
    输出：7 3 1 5 4
    描述：输入表示有6个节点，首节点的值为2，然后(1，2)表示在值为2的节点后添加值为1的元素，即得到2 -> 1；（3,2）即在2后添加3，得到2 -> 3 -> 1；
         （5,1）即在1的后面添加5，得到2 -> 3 -> 1 -> 5；（4,5）得到2 -> 3 -> 1 -> 5 -> 4；（7,2）得到2 -> 7 -> 3 -> 1 -> 5 -> 4
         最终的链表为：2 -> 7 -> 3 -> 1 -> 5 -> 4，需要删除的是值为2的节点，
         所以结果应为：7 -> 3 -> 1 -> 5 -> 4
 * 示例2：
    输入：5 2 3 2 4 3 5 2 1 4 3
    输出：2 5 4 1
 * 示例3：
    输入：1 5 5
    输出：null
 */
public class HJ48 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNext()) {
            int n = in.nextInt();
            // 定义链表，增删操作频繁的情况，使用LinkedList效率比较高
            List<Integer> chain = new LinkedList<>();
            // 初始化链表
            chain.add(in.nextInt());
            for (int i = 0; i < n - 1; i++) {
                int readyNode = in.nextInt();
                int existNode = in.nextInt();
                chain.add(chain.indexOf(existNode) + 1, readyNode);
            }
            // 删除指定值，注意这里与chain.remove(in.nextInt())的区别
            chain.remove(new Integer(in.nextInt()));
            // 输出
            if (chain.size() == 0) {
                System.out.println("null");
            } else {
                for (int i = 0; i < chain.size(); i++) {
                    System.out.print(chain.get(i) + (i == chain.size() - 1 ? "\n" : " "));  // 最后三目运算符这里必须加括号，并且返回的不能是字符必须是字符串
                }
            }
        }
    }
}
