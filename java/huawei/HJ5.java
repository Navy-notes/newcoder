package huawei;

import java.util.Scanner;

/**
 * @author Yuliang.Lee
 * @version 1.0
 * @date 2021/8/17 17:47
 * 十六进制转十进制
 */
public class HJ5 {

    public static double BASE = 16;
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNextLine()) {
            String hex = in.nextLine();
            int result = 0;
            int lastIndex = hex.length() - 1;
            // 从十六进制的低位开始往高位遍历（1<= 位数 <=4）
            for (int i = 0; lastIndex - i >= 0; i++) {
                char digit = hex.charAt(lastIndex - i);
                switch (digit) {
                    case 'x':
                        break;
                    case 'A':
                        result += 10 * Math.pow(BASE, i);
                        break;
                    case 'B':
                        result += 11 * Math.pow(BASE, i);
                        break;
                    case 'C':
                        result += 12 * Math.pow(BASE, i);
                        break;
                    case 'D':
                        result += 13 * Math.pow(BASE, i);
                        break;
                    case 'E':
                        result += 14 * Math.pow(BASE, i);
                        break;
                    case 'F':
                        result += 15 * Math.pow(BASE, i);
                        break;
                    default:
                        result += Integer.parseInt(String.valueOf(digit)) * Math.pow(BASE, i);
                }
            }
            System.out.println(result);
        }
    }

}
