package huawei;

import java.util.Scanner;

/**
 * @author Yuliang.Lee
 * @version 1.0
 * @date 2021/9/28 11:58
 * 截取字符串：
    输入一个字符串和一个整数k，截取字符串的前k个字符并输出
 */
public class HJ46 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNext()) {
            String str = in.next();
            int k = in.nextInt();
            System.out.println(str.substring(0, k));
        }
    }
}
