package huawei;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

/**
 * @author Yuliang.Lee
 * @version 1.0
 * @date 2021/8/22 12:16
 * 字符个数统计
   输入一行没有空格的字符串。输出 输入字符串 中范围在ASCII(0~127，包括0和127)字符的种数。
 */
public class HJ10 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNextLine()) {
            String input = in.nextLine();
            Set<Character> set = new HashSet();
            for (int i = 0; i < input.length(); i++) {
                char ch = input.charAt(i);
                if (ch > 127)
                    continue;
                set.add(ch);
            }
            System.out.println(set.size());
        }
    }
}
