package huawei;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * @author Yuliang.Lee
 * @version 1.0
 * @date 2021/8/28 11:38
 * 简单错误记录：
   开发一个简单错误记录功能小模块，能够记录出错的代码所在的文件名称和行号。

   处理：
    1、 记录最多8条错误记录，循环记录，最后只用输出最后出现的八条错误记录。对相同的错误记录只记录一条，但是错误计数增加。最后一个斜杠后面的带后缀名的部分（保留最后16位）和行号完全匹配的记录才做算是”相同“的错误记录。
    2、 超过16个字符的文件名称，只记录文件的最后有效16个字符；
    3、 输入的文件可能带路径，记录文件名称不能带路径。
    4、循环记录时，只以第一次出现的顺序为准，后面重复的不会更新它的出现时间，仍以第一次为准
   示例1
    输入：
    D:\zwtymj\xccb\ljj\cqzlyaszjvlsjmkwoqijggmybr 645
    E:\je\rzuwnjvnuz 633
    C:\km\tgjwpb\gy\atl 637
    F:\weioj\hadd\connsh\rwyfvzsopsuiqjnr 647
    E:\ns\mfwj\wqkoki\eez 648
    D:\cfmwafhhgeyawnool 649
    E:\czt\opwip\osnll\c 637
    G:\nt\f 633
    F:\fop\ywzqaop 631
    F:\yay\jc\ywzqaop 631
    输出：
    rzuwnjvnuz 633 1
    atl 637 1
    rwyfvzsopsuiqjnr 647 1
    eez 648 1
    fmwafhhgeyawnool 649 1
    c 637 1
    f 633 1
    ywzqaop 631 2
 */
public class HJ19 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Map<String, Integer> map = new LinkedHashMap<>();
        while (in.hasNextLine()) {
            String input = in.nextLine();
            if (input.length() == 0) {
                break;
            }
            String[] inputArr = input.split(" ");
            String[] split = inputArr[0].split("\\\\");
            String key = split[split.length - 1] + "\\" + inputArr[1];
            if (map.containsKey(key)) {
                map.put(key, map.get(key) + 1);
            } else {
                map.put(key, 1);
            }
        }
        // 输出(最多)后8条记录
        Object[] keyArr = map.keySet().toArray();
        int size = keyArr.length;
        int index = size <= 8 ? 0 : size - 8;
        while (index < size) {
            String[] key = String.valueOf(keyArr[index]).split("\\\\");
            String fileName = key[0];
            String row = key[1];
            // 文件名保留(最多)后16位
            int subIndex = fileName.length() <= 16 ? 0 : fileName.length() - 16;
            System.out.println(fileName.substring(subIndex) + " " + row + " " + map.get(keyArr[index]));
            index++;
        }
    }
}
