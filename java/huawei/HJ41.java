package huawei;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

/**
 * @author Yuliang.Lee
 * @version 1.0
 * @date 2021/9/18 11:03
 * 称砝码：
    现有一组砝码，重量互不相等，分别为m1,m2,m3…mn；
    每种砝码对应的数量为x1,x2,x3...xn。现在要用这些砝码去称物体的重量(放在同一侧)，问能称出多少种不同的重量。
    注：称重重量包括0
 * 解题关键：
    【意识到迭代逻辑，n个砝码能够称重的重量数一定是基于n-1个砝码称重数的基础上的】
        利用集合去重的性质，用set缓存能得到的称重重量，以示例为例，先在集合里面添加0，
        当第一个砝码进来的时候；
            {0} 变成 {0,0+1} -> {0,1}
        当第二个砝码进来之后；
            {0，1} 变成 {0，1，0+1，1+1} --> {0,1,2}
        当第三个砝码进来之后；
            {0,1,2} 变成{0，1，2，0+2，1+2，2+2} ---> {0,1,2,3,4}
        最终set集合中的元素个数即为能称出的重量种数。
 * 示例：
    输入：
        2
        1 2
        2 1
    输出：
        5
 */
public class HJ41 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNext()) {
            // 砝码种类数
            int n = in.nextInt();
            // 砝码重量
            int[] weight = new int[n];
            for (int i = 0; i < n; i++) {
                weight[i] = in.nextInt();
            }
            // 所有砝码的集合
            List<Integer> scales = new ArrayList<>();
            for (int i = 0; i < n; i++) {
                // 砝码不同重量对应的个数
                int quantity = in.nextInt();
                for (int j = 0; j < quantity; j++) {
                    scales.add(weight[i]);
                }
            }
            // 设能够称重的重量去重集合为set
            Set<Integer> set = new HashSet<>();
            // 初始化，集合需包含称重重量为0
            set.add(0);
            // 迭代逻辑：n个砝码能够称重的重量数一定是基于n-1个砝码称重数的基础上的
            for (Integer scale : scales) {
                int size = set.size();
                Integer[] tmp = new Integer[size];
                tmp = set.toArray(tmp);
                for (int i = 0; i < size; i++) {
                    set.add(tmp[i] + scale);
                }
            }
            // 输出总共能称出的重量数
            System.out.println(set.size());
        }
    }


//    /**
//     * 计算排列方式Cnr
//     * @param n
//     * @param r
//     * @return
//     */
//    public static int combination(int n, int r) {
//        int fn = 1;
//        for (int i = n; i > 0; i--) {
//            fn *= i;
//        }
//        int fr = 1;
//        for (int i = r; i > 0; i--) {
//            fr *= i;
//        }
//        int f_nr = 1;
//        for (int i = n - r; i > 0; i--) {
//            f_nr *= i;
//        }
//        return fn / (fr * f_nr);
//    }
}
