package huawei;

import java.util.Scanner;

/**
 * @author Yuliang.Lee
 * @version 1.0
 * @date 2021/8/16 11:44
 * 字符串分隔：
   连续输入字符串，请按长度为8拆分每个字符串后输出到新的字符串数组；
   长度不是8整数倍的字符串请在后面补数字0，空字符串不处理。
 */
public class HJ4 {
    public static int LEN = 8;

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        // 注意 while 处理多个 case
        while (in.hasNext()) {
            String orginStr = in.next();
            int size = orginStr.length();
            if (size == 0) continue;  //空字符串不处理
            for (int i = 0; i < size; i = i + LEN) {
                if (i + LEN <= size) {
                    System.out.println(orginStr.substring(i, i + LEN));
                }
                else {
                    int fill = LEN - orginStr.substring(i).length();  // 0<fill<8
                    System.out.print(orginStr.substring(i));
                    for (int j = 1; j <= fill; j++) {
                        System.out.print('0');
                    }
                    System.out.println();
                }
            }
        }
    }
}
