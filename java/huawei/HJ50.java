package huawei;

import java.util.Scanner;
import java.util.Stack;

/**
 * @author Yuliang.Lee
 * @version 1.0
 * @date 2021/9/23 18:37
 * 四则运算：
    输入一个表达式（用字符串表示），求这个表达式的值。
    保证字符串中的有效字符包括[‘0’-‘9’],‘+’,‘-’, ‘*’,‘/’ ,‘(’， ‘)’,‘[’, ‘]’,‘{’ ,‘}’。且表达式一定合法。

 * 解题思路：（栈、递归）
    第一步，先考虑无括号的情况，先乘除后加减，这个用栈很容易解决，遇到数字先压栈，如果下一个是乘号或除号，先出栈，和下一个数进行乘除运算，再入栈，最后就能保证栈内所有数字都是加数，最后对所有加数求和即可。
    （注：将所有的减号都看成是负数，乘除则出栈运算后再将结果入栈，所以最后数字栈中的所有元素之间的运算符必定都是加号）
    第二步，遇到左括号，直接递归执行第一步即可，最后检测到右括号，返回括号内的计算结果，退出函数，这个结果作为一个加数，返回上一层入栈。

 * 示例
    输入：3+2*{1+2*[-4/(8-6)+7]}
    输出：25
 */
public class HJ50 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNext()) {
            String expr = in.nextLine();
            System.out.println(calculate(expr, 0));
        }
    }

    public static String calculate(String expr, int index) {
        // 数字栈（包含负数）
        Stack<Integer> numbers = new Stack<>();
        // 考虑有负数和减号的情况
        boolean negative = false;
        // 考虑有多位数的情况
        StringBuilder tmp = new StringBuilder();
        // flag为1的时候即运算符为乘除时，等待运算；为0才可以允许本趟的数字压栈
        int flag = 0;
        char op = ' ';

        for (int i = index; i < expr.length(); i++) {
            char ch = expr.charAt(i);
            if (ch >= '0' && ch <= '9') {
                tmp.append(ch);
            }
            else {
                if (tmp.length() > 0) {
                    // 数字入栈
                    if (flag == 0 ) {
                        numbers.push(Integer.parseInt(negative ? ('-' + tmp.toString()) : tmp.toString()));
                        tmp.delete(0, tmp.length());
                        negative = false;
                    }
                    // 进行乘除运算
                    else {
                        int a = numbers.pop();
                        int b = Integer.parseInt(negative ? ('-' + tmp.toString()) : tmp.toString());
                        switch (op) {
                            case '*':
                                numbers.push(a * b);
                                break;
                            case '/':
                                numbers.push(a / b);
                                break;
                            default:
                                break;
                        }
                        tmp.delete(0, tmp.length());
                        negative = false;
                        flag = 0;
                        op = ' ';
                    }
                }
                // 处理本次字符
                if (ch == '-') {
                    negative = true;
                } else if (ch == '*' || ch == '/') {
                    flag = 1;
                    op = ch;
                } else if (ch == '{' || ch == '[' || ch == '(') {
                    // 递归调用函数
                    String[] middleResult = calculate(expr, i+1).split(",");
                    // 处理递归返回的结果
                    tmp.append(middleResult[0]);
                    i = Integer.parseInt(middleResult[1]);
                } else if (ch == '}' || ch == ']' || ch == ')') {
                    // 计算函数的结果值
                    int sum = 0;
                    for (Integer number : numbers) {
                        sum += number;
                    }
                    // 返回递归值
                    return sum + "," + i;
                }
            }
        }

        // 处理最后一次的数字和运算
        if (tmp.length() != 0) {
            int b = Integer.parseInt(negative ? ('-' + tmp.toString()) : tmp.toString());
            if (flag == 1) {
                int a = numbers.pop();
                switch (op) {
                    case '*':
                        numbers.push(a * b);
                        break;
                    case '/':
                        numbers.push(a / b);
                        break;
                    default:
                        break;
                }
            } else {
                numbers.push(b);
            }
        }
        // 返回最终的结果值
        int result = 0;
        for (Integer number : numbers) {
            result += number;
        }
        return result + "" ;
    }
}
