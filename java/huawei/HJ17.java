package huawei;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Yuliang.Lee
 * @version 1.0
 * @date 2021/8/26 11:26
 * 坐标移动:
    开发一个坐标计算工具， A表示向左移动，D表示向右移动，W表示向上移动，S表示向下移动。从（0,0）点开始移动，从输入字符串里面读取一些坐标，并将最终输入结果输出到输出文件里面。

    输入：
    合法坐标为A(或者D或者W或者S) + 数字（两位以内）
    坐标之间以;分隔。
    非法坐标点需要进行丢弃。如AA10;  A1A;  $%$;  YAD; 等。
    下面是一个简单的例子 如：
    A10;S20;W10;D30;X;A1A;B10A11;;A10;
    处理过程：
    起点（0,0）
    +   A10   =  （-10,0）
    +   S20   =  (-10,-20)
    +   W10  =  (-10,-10)
    +   D30  =  (20,-10)
    +   x    =  无效
    +   A1A   =  无效
    +   B10A11   =  无效
    +  一个空 不影响
    +   A10  =  (10,-10)
    结果 （10， -10）

    正则在线工具：http://tool.chinaz.com/regex/、https://www.sojson.com/regex/
    (?i) 表示所在位置右侧的表达式开启忽略大小写模式
    Matcher.group(int)方法详细解析： https://blog.csdn.net/Harden3/article/details/99463823
 */
public class HJ17 {
    public static String REGEX_STR = "[ADWS]\\d{1,2}";

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNext()) {
            String[] input = in.nextLine().split(";");
            int ad = 0;
            int ws = 0;

            /**
             * 参考博客：https://blog.csdn.net/dnc8371/article/details/107255428
             *
             * mathcher.matches()方法会对输入字符串进行全体匹配。（仅匹配一次）
             * matcher.find()方法会对输入字符串进行分步匹配。（会匹配多次）
             * 举个例子：REGEX_STR = "[ADWS]\d{1,2}" 搭配 mathcher.matches()使用
             *          等价于
             *          REGEX_STR = "^[ADWS]\d{1,2}$" 搭配  matcher.find()使用
             *
             * 调用matcher.group()或者mather.group(0)可以获取find()当次匹配到的字符串组或matches()当前匹配到的字符串。
             * group(index)可以获取该字符串组的子字符串组。index是从1开始计数的，根据正则表达式中的括号划分不同的index子组。
             */
            Pattern pattern = Pattern.compile(REGEX_STR);
            for (String coordinate : input) {
                Matcher matcher = pattern.matcher(coordinate);
                if (matcher.matches())  {
                    char direction = coordinate.charAt(0);
                    int value = Integer.parseInt(coordinate.substring(1));
                    switch (direction) {
                        case 'A':
                            ad -= value;
                            break;
                        case 'D':
                            ad += value;
                            break;
                        case 'W':
                            ws += value;
                            break;
                        case 'S':
                            ws -= value;
                            break;
                        default:
                            break;
                    }
                }
            }
            System.out.println(ad + "," + ws);
        }
    }
}
