package huawei;

import java.util.Scanner;

/**
 * @author Yuliang.Lee
 * @version 1.0
 * @date 2021/9/21 16:07
 * 迷宫问题：
    定义一个二维数组N*M（其中2<=N<=10;2<=M<=10），如5 × 5数组下所示：
        int maze[5][5] = {
        0, 1, 0, 0, 0,
        0, 1, 1, 1, 0,
        0, 0, 0, 0, 0,
        0, 1, 1, 1, 0,
        0, 0, 0, 1, 0,
        };
    它表示一个迷宫，其中的1表示墙壁，0表示可以走的路，只能横着走或竖着走，不能斜着走，要求编程序找出从左上角到右下角的最短路线。入口点为[0,0],既第一格是可以走的路。

 * 深度优先搜索DFS+回溯法： https://www.bilibili.com/video/BV1bK4y1C7W2?share_source=copy_web
   广度优先搜索BFS： https://www.bilibili.com/video/BV16C4y1s7EF?share_source=copy_web

 * 本题其实是简化的迷宫问题，但是依然可以使用DFS或BFS解决，不过考虑到需要打印最短路线的，所以采用DFS+回溯是比较好的。
   如果不要求输出路径只求最短长度的化，则采用BFS更容易实现。
 */
public class HJ43 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNext()) {

        }
    }
}
