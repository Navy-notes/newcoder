package huawei;

import java.util.Scanner;

/**
 * @author Yuliang.Lee
 * @version 1.0
 * @date 2021/9/23 18:00
 * 计算字符串的距离：
    Levenshtein 距离，又称编辑距离，指的是两个字符串之间，由一个转换成另一个所需的最少编辑操作次数。
    许可的编辑操作包括将一个字符替换成另一个字符，插入一个字符，删除一个字符。
    编辑距离的算法是首先由俄国科学家Levenshtein提出的，故又叫Levenshtein Distance。
    Ex：
    字符串A: abcdefg
    字符串B: abcdef
    通过增加或是删掉字符”g”的方式达到目的。这两种方案都需要一次操作。把这个操作所需要的次数定义为两个字符串的距离。
    要求：给定任意两个字符串，写出一个算法计算它们的编辑距离。

 * 解题思路：
    这题考的是levenshtein距离的计算，需要运用动态规划去解决该类问题。
    迭代公式：
        lev[i][j]用来表示字符串a的[1...i]和字符串b[1...j]的levenshtein距离；
        插入和删除操作互为逆过程：a删除指定字符变b等同于b插入指定字符变a；
        如果a[i] == b[j]，则说明a[i]和b[j]分别加入a，b之后不会影响levenshtein距离，lev[i][j] = lev[i-1][j-1] + 0;
        如果a[i] != b[j]，则需要考虑3种情况的可能：
            a中插入字符，即lev[i][j] = lev[i-1][j] + 1;
            b中插入字符，即lev[i][j] = lev[i][j-1] + 1;
            a[i]替换成b[j]，lev[i][j] = lev[i-1][j-1] + 1;
        取这3种情况的最小值。
    细节补充：
        二维数组的初始化以及迭代算法的修正体现在代码实现中。

 * 示例：
    输入：
        abcdefg
        abcdef
        abcde
        abcdf
        abcde
        bcdef
    输出：
        1
        1
        2
 */
public class HJ52 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNext()) {
            String str1 = in.nextLine();
            String str2 = in.nextLine();
            int len1 = str1.length();
            int len2 = str2.length();
            int[][] lev = new int[len1][len2];
            // 初始化
            if (str1.charAt(0) == str2.charAt(0)) {
                lev[0][0] = 0;
            } else {
                lev[0][0] = 1;
            }
            // dp填充数组
            for (int i = 0; i < len1; i++) {
                char a = str1.charAt(i);
                for (int j = 0; j < len2; j++) {
                    char b = str2.charAt(j);
                    if (a == b) {
                        if (i > 0 && j > 0) {
                            lev[i][j] = lev[i-1][j-1];
                        }
                    }
                    else if (i > 0 || j > 0) {
                        int min = Integer.MAX_VALUE;
                        if (i > 0 && lev[i-1][j] + 1 < min) {
                            min = lev[i-1][j] + 1;
                        }
                        if (j > 0 && lev[i][j-1] + 1 < min) {
                            min = lev[i][j-1] + 1;
                        }
                        if (i > 0 && j > 0 && lev[i-1][j-1] + 1 < min) {
                            min = lev[i-1][j-1] + 1;
                        }
                        lev[i][j] = min;
                    }
                    // 算法校正：levenshtein距离的最小值为两个字符串长度之差
                    lev[i][j] = lev[i][j] < Math.abs(i - j) ? Math.abs(i - j) : lev[i][j];
                }
            }
            // 结果输出
            System.out.println(lev[len1 - 1][len2 - 1]);
        }
    }
}
