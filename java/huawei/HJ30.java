package huawei;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

/**
 * @author Yuliang.Lee
 * @version 1.0
 * @date 2021/9/8 12:25
 * 字符串合并处理：
    将输入的两个字符串合并。
    对合并后的字符串进行排序，要求为：下标为奇数的字符和下标为偶数的字符分别从小到大排序。这里的下标意思是字符在字符串中的位置。
    对排序后的字符串进行操作，如果字符为‘0’——‘9’或者‘A’——‘F’或者‘a’——‘f’，则对他们所代表的16进制的数进行BIT倒序的操作，并转换为相应的大写字符。如字符为‘4’，为0100b，则翻转后为0010b，也就是2。转换后的字符为‘2’； 如字符为‘7’，为0111b，则翻转后为1110b，也就是e。转换后的字符为大写‘E’。

 * 举例：
    输入str1为"dec"，str2为"fab"，合并为“decfab”，分别对“dca”和“efb”进行排序，排序后为“abcedf”，
    转换后为“5D37BF”
 */
public class HJ30 {
    public static void main(String[] args) {
        // 映射表的定义
        char[] mapped = new char[16];
        // 运行得到十六进制的转换映射结果
        for (int i = 0; i < 16; i++) {
            int result = Integer.parseInt(new StringBuilder(String.format("%04d", Integer.parseInt(Integer.toBinaryString(i)))).reverse().toString(), 2);
            // 转换为大写字母
            if (result >= 10 && result <= 15) {
                result += 55;
            }
            // 转换为字符型的数字
            else {
                result += 48;
            }
            mapped[i] = (char) result;
        }
        // 正式开始对题目的输入进行处理
        Scanner in = new Scanner(System.in);
        while (in.hasNext()) {
            String str = in.next().concat(in.next());
            // 偶数下标的字符 0,2,4,6...
            List<Character> list1 = new ArrayList<>();
            // 奇数下标的字符 1,3,5,7...
            List<Character> list2 = new ArrayList<>();
            for (int i = 0; i < str.length(); i = i + 2) {
                list1.add(str.charAt(i));
                if (i + 1 < str.length()) {
                    list2.add(str.charAt(i+1));
                }
            }
            // 完成奇偶两边的排序并再次合并
            Collections.sort(list1);
            Collections.sort(list2);
            char[] arr3 = new char[str.length()];
            int k = 0;
            for (int j = 0; j < str.length() / 2 + 1; j++) {
                if (j < list1.size()) {
                    arr3[k] = list1.get(j);
                }
                if (j < list2.size()) {
                    arr3[k + 1] = list2.get(j);
                }
                k += 2;
            }
            // 转十六进制，并做倒序处理（实际上对照映射表的缓存读取即可）
            for (int i = 0; i < arr3.length; i++) {
                if (arr3[i] >= '0' && arr3[i] <= '9') {
                    arr3[i] = mapped[arr3[i] - 48];
                } else if (arr3[i] >= 'A' && arr3[i] <= 'F') {
                    arr3[i] = mapped[arr3[i] - 55];
                } else if (arr3[i] >= 'a' && arr3[i] <= 'f') {
                    arr3[i] = mapped[arr3[i] - 87];
                }
                System.out.print(arr3[i]);
            }
            System.out.println();
        }
    }
}
