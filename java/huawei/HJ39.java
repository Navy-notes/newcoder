package huawei;

import java.util.Scanner;

/**
 * @author Yuliang.Lee
 * @version 1.0
 * @date 2021/9/17 12:45
 * 判断两个IP是否属于同一个子网：
    对于第一个例子:
    255.255.255.0
    192.168.224.256
    192.168.10.4
    其中IP:192.168.224.256不合法，输出1

    对于第二个例子:
    255.0.0.0
    193.194.202.15
    232.43.7.59
    2个与运算之后，不在同一个子网，输出2

    对于第三个例子，
    255.255.255.0
    192.168.0.254
    192.168.0.1
    2个与运算之后，如题目描述所示，在同一个子网，输出0
 */
public class HJ39 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNext()) {
            String[] mask = in.nextLine().split("\\.");
            String[] ip1 = in.nextLine().split("\\.");
            String[] ip2 = in.nextLine().split("\\.");
            if (!checkMask(mask) || !checkIp(ip1) || !checkIp(ip2)) {
                System.out.println(1);
                continue;
            }
            if (isSameNetwork(mask, ip1, ip2)) {
                System.out.println(0);
            } else {
                System.out.println(2);
            }
        }
    }

    /**
     * 校验掩码合法性
     * @param mask
     * @return
     */
    public static boolean checkMask(String[] mask) {
        int flag = 1;
        for (int i = 0; i < mask.length; i++) {
            int e = Integer.parseInt(mask[i]);
            if (flag == 1) {
                if (e == 255)
                    continue;
                if (e != 0 && e != 128 && e != 192 && e != 224 && e != 240
                        && e != 248 && e != 252 && e != 254) {
                    return false;
                }
                flag = 0;
            }
            else {
                if (e != 0) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * 校验IP合法性
     * @param ip
     * @return
     */
    public static boolean checkIp(String[] ip) {
        for (int i = 0; i < ip.length; i++) {
            int e = Integer.parseInt(ip[i]);
            if (e > 255 || e < 0) {
                return false;
            }
        }
        return true;
    }

    /**
     * 判断两个合法IP地址是否为同一个网段
     * @param mask
     * @param ip_1
     * @param ip_2
     * @return
     */
    public static boolean isSameNetwork(String[] mask, String[] ip1, String[] ip2) {
        for (int i = 0; i < mask.length; i++) {
            int mask_i = Integer.parseInt(mask[i]);
            int ip1_i = Integer.parseInt(ip1[i]);
            int ip2_i = Integer.parseInt(ip2[i]);
            if ((mask_i & ip1_i) != (mask_i & ip2_i)) {
                return false;
            }
        }
        return true;
    }
}
