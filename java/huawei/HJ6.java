package huawei;

import java.util.Scanner;

/**
 * @author Yuliang.Lee
 * @version 1.0
 * @date 2021/8/18 11:57
 * 质数因子：
   输入一个正整数，按照从小到大的顺序输出它的所有质因子（重复的也要列举）
  （如180的质因子为2 2 3 3 5 ）最后一个数后面也要有空格
 */
public class HJ6 {
//    // while循环实现，时间复杂度O(n)
//    public static void main(String[] args) {
//        Scanner in = new Scanner(System.in);
//        while (in.hasNextLong()) {
//            long input = in.nextLong();   // 输入：大于等于2的正整数
//            int factor = 2;
//            while (input >= factor) {
//                if (input % factor == 0) {
//                    System.out.print(factor + " ");
//                    input /= factor;
//                }
//                else {
//                    factor++;
//                }
//            }
//            System.out.println();
//        }
//    }

    // 利用开平方优化循环的上边界，时间复杂度O(sqrt(n))
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNextLong()) {
            long input = in.nextLong();   // 输入：大于等于2的正整数
            int border = (int) Math.sqrt(input);
            int factor = 2;
            while (factor <= border) {
                if (input % factor == 0) {
                    System.out.print(factor + " ");
                    input /= factor;
                }
                else {
                    factor++;
                }
            }
            // 如果剩下的最后一个因数是质数，并且溢出了上边界，则需要补上其自身的这种情况
            if (input != 1) {
                System.out.print(input + " ");
            }
            System.out.println();
        }
    }
}
