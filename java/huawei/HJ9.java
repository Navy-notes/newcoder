package huawei;

import java.util.LinkedHashSet;
import java.util.Scanner;
import java.util.Set;

/**
 * @author Yuliang.Lee
 * @version 1.0
 * @date 2021/8/21 11:35
 * 提取不重复的整数：
   输入一个int型整数，按照从右向左的阅读顺序，返回一个不含重复数字的新的整数。
   保证输入的整数最后一位不是0。
  【示例】
   输入：
    9876673
   输出：
    37689
 */
public class HJ9 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNextLine()) {
            String input = in.nextLine();
            Set<Character> set = new LinkedHashSet<>();
            for (int i = input.length() - 1; i >= 0; i--) {
                set.add(input.charAt(i));
            }
            for (Character chr: set) {
                System.out.print(chr);
            }
            System.out.println();
        }
    }
}
