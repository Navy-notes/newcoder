package huawei;

import java.util.Scanner;

/**
 * @author Yuliang.Lee
 * @version 1.0
 * @date 2021/8/23 10:28
 * 句子逆序：
   输入 I am a boy
   输出 boy a am I
 */
public class HJ13 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNextLine()) {
            String input = in.nextLine();
            StringBuilder sb = new StringBuilder(input.length());
            String[] arr = input.split(" ");
            int i = arr.length - 1;
            while (i >= 0) {
                sb.append(arr[i]).append(" ");
                i--;
            }
            System.out.println(sb.substring(0,sb.length() - 1));
        }
    }
}
