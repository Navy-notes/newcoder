package huawei;

import java.util.Scanner;

/**
 * @author Yuliang.Lee
 * @version 1.0
 * @date 2021/9/18 10:03
 * 统计字符：
    输入一行字符，分别统计出包含英文字母、空格、数字和其它字符的个数。

  输入：
    1qazxsw23 edcvfr45tgbn hy67uj m,ki89ol.\\/;p0-=\\][
  输出：
    26
    3
    10
    12
 */
public class HJ40 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNext()) {
            String input = in.nextLine();
            int letterNum = 0;
            int blankNum = 0;
            int digitNum = 0;
            int otherNum = 0;
            for (int i = 0; i < input.length(); i++) {
                char ch = input.charAt(i);
                if ((ch >= 'A' && ch <= 'Z') || (ch >= 'a' && ch <= 'z')) {
                    letterNum++;
                } else if (ch == ' ') {
                    blankNum++;
                } else if (ch >= '0' && ch <= '9') {
                    digitNum++;
                } else {
                    otherNum++;
                }
            }
            System.out.println(letterNum);
            System.out.println(blankNum);
            System.out.println(digitNum);
            System.out.println(otherNum);
        }
    }
}
