package huawei;

import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

/**
 * @author Yuliang.Lee
 * @version 1.0
 * @date 2021/8/13 11:09
 * 随机数的去重和排序
 * Set集合的有关知识参考：https://www.cnblogs.com/chaoyang123/p/11548766.html
 */
public class HJ3 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Set<Integer> set = new TreeSet<>();  // TreeSet实现了SortedSet接口，默认按照整型升序排序
        while (in.hasNextInt()) {
            int ranNum = in.nextInt();
            for (int i = 1; i <= ranNum; i++) {
                set.add(in.nextInt());
            }
            for (Integer e: set) {
                System.out.println(e);
            }
            set.clear();
        }
    }

}
