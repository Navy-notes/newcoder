package huawei;

import java.util.Scanner;

/**
 * @author Yuliang.Lee
 * @version 1.0
 * @date 2021/9/22 17:59
 * 杨辉三角的变形：
              1
            1 1 1
          1 2 3 2 1
       1 3 6 7 6 3 1
    1 4 10 16 19 16 10 4 1
    以上三角形的数阵，第一行只有一个数1，以下每行的每个数，是恰好是它上面的数，左上角数到右上角的数，3个数之和（如果不存在某个数，认为该数就是0）。
    求第n行第一个偶数出现的位置。如果没有偶数，则输出-1。例如输入3,则输出2，输入4则输出3。

 * 示例：
    输入:
        4
        2
    输出：
        3
        -1
 */
public class HJ53 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNext()) {
            int rowNum = in.nextInt();
            // 定义二维数组
            int colNum = 2 * rowNum - 1;
            int[][] arr = new int[rowNum][colNum];
            // 初始化首行
            for (int i = 0; i < colNum; i++) {
                arr[0][i] = 0;
                if (i == colNum / 2) {
                    arr[0][i] = 1;
                }
            }
            // 填充数组
            for (int i = 1; i < rowNum; i++) {
                for (int j = 0; j < colNum; j++) {
                    if (j == 0) {
                        arr[i][j] = arr[i-1][j] + arr[i-1][j+1];
                    } else if (j == colNum - 1) {
                        arr[i][j] = arr[i-1][j-1] + arr[i-1][j];
                    } else {
                        arr[i][j] = arr[i-1][j-1] + arr[i-1][j] + arr[i-1][j+1];
                    }
                }
            }
            // 找最后一行的数字中第一次出现偶数的位置
            int result = -1;
            for (int j = 0; j < colNum; j++) {
                if (arr[rowNum - 1][j] != 0 && arr[rowNum - 1][j] % 2 == 0) {
                    result = j + 1;
                    break;
                }
            }
            System.out.println(result);
        }
    }
}
