package huawei;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

/**
 * @author Yuliang.Lee
 * @version 1.0
 * @date 2021/9/5 11:40
 * 查找兄弟单词
   定义一个单词的“兄弟单词”为：交换该单词字母顺序（注：可以交换任意次，至少1次），而不添加、删除、修改原有的字母就能生成的单词。
   兄弟单词要求和原来的单词不同。
   例如：ab和ba是兄弟单词。ab和ab则不是兄弟单词。

 * 示例
    输入：
    6 cab ad abcd cba abc bca abc 1
    输出：
    3
    bca
    说明：
    abc的兄弟单词有cab cba bca，所以输出3
    经字典序排列后，变为bca cab cba，所以第1个字典序兄弟单词为bca
 */
public class HJ27 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNext()) {
            // 输入数据初始化
            int n = in.nextInt();
            String[] dist = new String[n];
            for (int i = 0; i < n; i++) {
                dist[i] = in.next();
            }
            String xWord = in.next();
            char[] xWordArr = xWord.toCharArray();
            Arrays.sort(xWordArr);
            int k = in.nextInt();
            List<String> broWords = new ArrayList<>();

            // 查找兄弟单词
            for (String word : dist) {
                if (xWord.length() == word.length() && !xWord.equals(word)) {
                     // 将无序字符统一转为有序字符后再进行比较
                     char[] tmpWordArr = word.toCharArray();
                     Arrays.sort(tmpWordArr);
                     // 比较两个字符数组是否相等
                     if (Arrays.equals(tmpWordArr, xWordArr)) {
                         broWords.add(word);
                     }
                }
            }

            // 结果输出
            System.out.println(broWords.size());
            if (broWords.size() >= k) {
                Collections.sort(broWords);
                System.out.println(broWords.get(k - 1));
            }
        }
    }
}
