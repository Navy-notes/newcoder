package huawei;

import java.util.Scanner;

/**
 * @author Yuliang.Lee
 * @version 1.0
 * @date 2021/9/20 11:24
 * 学英语：
    Jessi初学英语，为了快速读出一串数字，编写程序将数字转换成英文：
    如22：twenty two，123：one hundred and twenty three。
    说明：
    数字为正整数，长度不超过九位，不考虑小数，转化结果为英文小写；
    输出格式为twenty two；
    非法数据请返回“error”；
    关键字提示：and，billion，million，thousand，hundred。

 * 解题思路：
    a.先判断输入的数是否不超过9位，且不能有负号，不能含小数点，然后用0补足得到9位字符数组。
    b.根据英文的数字表示法，每3个数字分隔为一组来处理，从左往右看：
        1~3位数，计量单位为million
        4~6位数，计量单位为thousand
        7~9位数，计量单位为hundred and 十位-个位 （注意对10-19的英文单词额外处理）
    c.million和thousand单位中的个十百的计数法则同7~9位数。
    d.最后将整合后的结果进行输出。

 * 示例：
    输入：2356 （2,356）
    输出：two thousand three hundred and fifty six

    输入：123000123 （123,000,123）
    输出：one hundred and twenty three million one hundred and twenty three

    输入：1000000 （1,000,000）
    输出：one million
 */
public class HJ42 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNext()) {
            String figure = in.nextLine();
            if (isLegal(figure)) {
                char[] digits = String.format("%09d", Integer.parseInt(figure)).toCharArray();
                StringBuilder output = new StringBuilder();
                for (int i = 0; i < 9; i += 3) {
                    String tmpResult = transferHundredsDigit(digits, i, i + 3);
                    if (tmpResult != null && tmpResult.length() != 0) {
                        output.append(tmpResult);
                        switch (i) {
                            case 0:
                                output.append("million ");
                                break;
                            case 3:
                                output.append("thousand ");
                                break;
                            case 6:
                                output.append(" ");
                                break;
                            default:
                                break;
                        }
                    }
                }
                if (output.length() == 0) {
                    output.append("zero ");
                }
                System.out.println(output.substring(0, output.length() - 1));
            }
            else {
                System.out.println("error");
            }
        }
    }


    /**
     * 合法性校验
     * @param figure
     * @return
     */
    public static boolean isLegal(String figure) {
        if (figure.length() > 9
                || figure.contains("-")
                || figure.contains(".")) {
            return false;
        }
        return true;
    }


    /**
     * 个十百的转换
     * @param digits
     * @return
     */
    public static String transferHundredsDigit(char[] digits, int start, int end) {
        boolean hasAnd = false;
        boolean isTeen = false;
        StringBuilder result = new StringBuilder();
        for (int i = start; i < end; i++) {
            // 当十位为1时
            if (isTeen) {
                result.append(transferTeen(digits[end - 1]));
                break;
            }
            // 百位和个位
            if (i == start || i == end - 1) {
                switch (digits[i]) {
                    case '0':
                        break;
                    case '1':
                        result.append("one ");
                        break;
                    case '2':
                        result.append("two ");
                        break;
                    case '3':
                        result.append("three ");
                        break;
                    case '4':
                        result.append("four ");
                        break;
                    case '5':
                        result.append("five ");
                        break;
                    case '6':
                        result.append("six ");
                        break;
                    case '7':
                        result.append("seven ");
                        break;
                    case '8':
                        result.append("eight ");
                        break;
                    case '9':
                        result.append("nine ");
                        break;
                    default:
                        break;
                }
                if (i == start && digits[i] != '0') {
                    hasAnd = true;
                    result.append("hundred ");
                }
            }
            // 十位
            else {
                if (hasAnd && (digits[i] != '0' || digits[i + 1] != '0')) {
                    result.append("and ");
                }
                switch (digits[i]) {
                    case '0':
                        break;
                    case '1':
                        isTeen = true;
                        break;
                    case '2':
                        result.append("twenty ");
                        break;
                    case '3':
                        result.append("thirty ");
                        break;
                    case '4':
                        result.append("forty ");
                        break;
                    case '5':
                        result.append("fifty ");
                        break;
                    case '6':
                        result.append("sixty ");
                        break;
                    case '7':
                        result.append("seventy ");
                        break;
                    case '8':
                        result.append("eighty ");
                        break;
                    case '9':
                        result.append("ninety ");
                        break;
                    default:
                        break;
                }
            }
        }
        return result.toString();
    }


    /**
     * 10~19对应的英文单词转译
     * @param number
     * @return
     */
    public static String transferTeen(char number) {
        switch (number) {
            case '0':
                return "ten ";
            case '1':
                return "eleven ";
            case '2':
                return "twelve ";
            case '3':
                return "thirteen ";
            case '4':
                return "fourteen ";
            case '5':
                return "fifteen ";
            case '6':
                return "sixteen ";
            case '7':
                return "seventeen ";
            case '8':
                return "eighteen ";
            case '9':
                return "nineteen ";
            default:
                return "";
        }
    }
}
