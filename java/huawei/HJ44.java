package huawei;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * @author Yuliang.Lee
 * @version 1.0
 * @date 2021/9/29 12:29
 * Sudoku：
    问题描述：数独（Sudoku）是一款大众喜爱的数字逻辑游戏。玩家需要根据9X9盘面上的已知数字，推算出所有剩余空格的数字，并且满足每一行、每一列、每一个3X3粗线宫内的数字均含1-9，并且不重复。
    输入用例包含已知数字的9X9盘面数组[空缺位以数字0表示]，应保证只有唯一解。
    （1+2+3+4+5+...+9 = 45）
 * 示例：
    输入：
    0 9 2 4 8 1 7 6 3
    4 1 3 7 6 2 9 8 5
    8 6 7 3 5 9 4 1 2
    6 2 4 1 9 5 3 7 8
    7 5 9 8 4 3 1 2 6
    1 3 8 6 2 7 5 9 4
    2 7 1 5 3 8 6 4 9
    3 8 6 9 1 4 2 5 7
    0 4 5 2 7 6 8 3 1
    输出：
    5 9 2 4 8 1 7 6 3
    4 1 3 7 6 2 9 8 5
    8 6 7 3 5 9 4 1 2
    6 2 4 1 9 5 3 7 8
    7 5 9 8 4 3 1 2 6
    1 3 8 6 2 7 5 9 4
    2 7 1 5 3 8 6 4 9
    3 8 6 9 1 4 2 5 7
    9 4 5 2 7 6 8 3 1
 */
public class HJ44 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNext()) {
            // 数独
            int[][] sudoku = new int[9][9];
            // 空缺位置信息：记录0所在的行和列，用逗号间隔
            List<String> vacancy = new ArrayList<>();
            // 初始化
            for (int i = 0; i < 9; i++) {
                for (int j = 0; j < 9; j++) {
                    int number = in.nextInt();
                    if (number == 0) {
                        vacancy.add(i + "," + j);
                    }
                    sudoku[i][j] = number;
                }
            }
            // 填充数独
            List<String> restVacancy = new ArrayList<>();
            while (true) {
                if (vacancy.size() == 0) {
                    break;
                }
                for (String s : vacancy) {
                    // 根据空缺信息定位到需要填充的行或列
                    String[] split = s.split(",");
                    int row = Integer.parseInt(split[0]);
                    int col = Integer.parseInt(split[1]);
                    // 根据空缺元素所在的行或列填充
                    if (!rowSearch(row, col, sudoku) && !colSearch(row, col, sudoku)) {
                        restVacancy.add(s);
                    }
                }
                // 清空列表
                vacancy.clear();
                // 交换内存
                List<String> tmp = restVacancy;
                restVacancy = vacancy;
                vacancy = tmp;
            }
            // 结果输出
            for (int[] arr : sudoku) {
                for (int i = 0; i < 9; i++) {
                    System.out.print(arr[i] + (i == 8 ? "\n" : " "));
                }
            }
        }
    }

    /**
     * 遍历指定的行填充空缺元素
     * @param row
     * @param col
     * @param sudoku
     * @return
     */
    private static boolean rowSearch(int row, int col, int[][] sudoku) {
        int sum = 0;
        for (int j = 0; j < 9; j++) {
            // 当同一行中存在两个空缺元素时，无法确定唯一解，暂时搁置应返回false
            if (sudoku[row][j] == 0 && j != col ) {
                return false;
            }
            // 计算同一行所有元素的累加值
            sum += sudoku[row][j];
        }
        // 填充唯一的空缺元素
        sudoku[row][col] = 45 - sum;
        return true;
    }

    /**
     * 遍历指定的列填充空缺元素
     * @param row
     * @param col
     * @param sudoku
     * @return
     */
    private static boolean colSearch(int row, int col, int[][] sudoku) {
        int sum = 0;
        for (int i = 0; i < 9; i++) {
            // 当同一列中存在两个空缺元素时，无法确定唯一解，暂时搁置应返回false
            if (sudoku[i][col] == 0 && i != row ) {
                return false;
            }
            // 计算同一列所有元素的累加值
            sum += sudoku[i][col];
        }
        // 填充唯一的空缺元素
        sudoku[row][col] = 45 - sum;
        return true;
    }
}
