package huawei;

import java.util.Scanner;

/**
 * @author Yuliang.Lee
 * @version 1.0
 * @date 2021/9/9 10:40
 * 最长的对称字符串：
    比如像这些ABBA，ABA，A，123321，称为对称字符串，定义为有效密码。
    输入一个字符串，返回有效密码串的最大长度。
 * 示例：
    输入1：1kABBA21
    输出1：4
    输入2：abaaab
    输出2：5
 */
public class HJ32 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNext()) {
            String pswd = in.nextLine();
            int maxValid = 0;
            for (int n = pswd.length(); n > 0; n--) {
                if (hasValidWithinLen(pswd, n)) {
                    maxValid = n;
                    break;
                }
            }
            System.out.println(maxValid);
        }
    }

    // 判断是否存在长度为len的对称字符串
    public static boolean hasValidWithinLen(String password, int len) {
        for (int i = 0; i + len - 1 < password.length(); i++) {
            int left = i;
            int right = i + len - 1;
            while (left < right) {
                // 指针从最左侧最右侧的字符往中间对称轴靠，如果当中有任意一对左右字符不相等则结束本次对称判断
                if (password.charAt(left) != password.charAt(right)) {
                    break;
                }
                left++;
                right--;
            }
            if (left >= right) {
                return true;
            }
        }
        return false;
    }
}

///*
// * 动态规划算法
// * dp(i, j) 表示是否 s(i ... j) 能够形成一个回文字符串
// * 当 s(i) 等于 s(j) 并且  s(i+1 ... j-1) 是一个回文字符串时 dp(i, j) 的取值为 true
// * 迭代式：dp[i][j] = s.charAt(i) == s.charAt(j) && (j - i < 3 || dp[i + 1][j - 1])
// * 当我们找到一个回文子字符串时，我们检查其是否为最长的回文字符串
// */
//public class HJ32 {
//    public static void main(String[] args) {
//        Scanner sc = new Scanner(System.in);
//        while(sc.hasNext()){
//            String s = sc.nextLine();
//            String res = longestPalindrome(s);
//            System.out.println(res.length());
//        }
//
//    }
//    public static String longestPalindrome(String s) {
//        // n表示字符串的长度
//        int n = s.length();
//        String res = null;
//        // 定义一个dp数组
//        boolean[][] dp = new boolean[n][n];
//        // 外层循环控制从最后一个字符向第一个字符渐进
//        for (int i = n - 1; i >= 0; i--) {
//            // 内层循环控制
//            for (int j = i; j < n; j++) {
//                // dp数组元素
//                dp[i][j] = s.charAt(i) == s.charAt(j) && (j - i < 3 || dp[i + 1][j - 1]);
//
//                if (dp[i][j] && (res == null || j - i + 1 > res.length())) {
//                    res = s.substring(i, j + 1);
//                }
//            }
//        }
//        return res;
//    }
//}
