package huawei;

import java.util.Scanner;

/**
 * @author Yuliang.Lee
 * @version 1.0
 * @date 2021/8/31 11:03
 * 汽水瓶：
   有这样一道智力题：“某商店规定：三个空汽水瓶可以换一瓶汽水。小张手上有十个空汽水瓶，她最多可以换多少瓶汽水喝？”
   答案是5瓶，方法如下：
   先用9个空瓶子换3瓶汽水，喝掉3瓶满的，喝完以后4个空瓶子，用3个再换一瓶，喝掉这瓶满的，这时候剩2个空瓶子。
   然后你让老板先借给你一瓶汽水，喝掉这瓶满的，喝完以后用3个空瓶子换一瓶满的还给老板。
   如果小张手上有n个空汽水瓶，最多可以换多少瓶汽水喝？

 * 示例1
    输入：
    3
    10
    81
    0
    输出：
    1
    5
    40
 */
public class HJ22 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNext()) {
            int n = in.nextInt();
            if (n == 0)
                break;
            int result = 0;
            int drinked = 0;
            int bottle = 0;
            /*
            也可以考虑用递归的思路去解决
            递归问题：
                3个瓶子换1瓶水+1个空瓶子，两个瓶子换1瓶水+0个空瓶子，1个瓶子换0瓶水。
                f(1) = 0
                f(2) = 1
                f(3) = 1
                f(4) = f(2)+1    //4个瓶子，其中3个可以换1瓶水+1个空瓶，所以是f(2)+1
                f(5) = f(3)+1    //3个瓶子换1瓶水+1个空瓶，所以是f(3)+1
                ...
                f(n) = f(n-2)+1
            */
            while (n / 3 != 0) {
                drinked = n / 3;
                bottle = n % 3;
                n = drinked + bottle;
                result += drinked;
            }
            if (n == 2) {
                result += 1;
            }
            System.out.println(result);
        }
    }
}
