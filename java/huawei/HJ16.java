package huawei;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * @author Yuliang.Lee
 * @version 1.0
 * @date 2021/8/25 13:39
 * 动态规划：核心是找到递推公式可供自底向上的迭代计算
    【背包问题】
        假设f(n,i)表示当背包容量为n时，前i个物品装背包的最大价值（也即最优方案的结果）
        V(i)表示第i个物品的体积
        P(i)表示第i个物品的价值
        逻辑：求f(n,i)的时候，即对第i个物品考虑是否入包的时候，首先判断能不能，再判断要不要。
        步骤：
            a.如果 V(i) > n，也就是说物品i即使单放也已经超过了背包的总容量，所以不能入包，
              那么 f(n,i) = f(n,i-1)
            b.如果 V(i) <= n，也就是物品i是能够放进包的，那么接下来我们再看要不要入包。
              b1. 假设物品i放入包，那么需要独占V(i)的空间，背包剩下的空间该如何安排才能达到最优？
                  很显然，f(n,i) = P(i) + f(n-V(i), i-1)
              b2. 如果物品i不放入包，那么前i个物品的最大价值依然为前一项的值，
                  即，   f(n,i) = f(n,i-1)
              b3. 最后比较b1和b2所计算出的最大价值，取最优的情况，即为情景b的f(n,i)的结果。
                         f(n,i) = max(b1,b2)
            c.最终，f(n,i)该项的值取情景a或者情景b的计算结果。
    【购物单问题】
        通过类比的思想，根据背包问题举一反三。但是这里由于存在主件和附件的依赖关系，所以复杂些，可归类为有依赖的背包问题。
        物品的价值等于价格*重要度，我们可以在数据输入的时候就做一些处理，屏蔽掉重要度这个参数。
        在处理输入数据的时候，我们把附件商品都整合到其对应的主件里去。假如输入的商品有m件，其中附件有a件，那么主件有m-a件。
        因为买主件不一定买附件，买附件一定买主件。只有当买主件的时候才会考虑附件，所以可以将问题简化为主件商品的问题。
        那么原问题可以转换为：
            “求在N元预算下，选购前m-a个主件商品的最大价值之和”
        选购前i个主件的最大价值之和是第一层背包问题，当购买第i个主件时，如何选购附件才能价值最大化是第二层背包问题。
        解决背包问题还是用我自己总结的两个大思路：
            “能不能”和“要不要”
        第一层背包问题：
            首先判断主件i能不能买，如果能买再判断要不要买。
        第二层背包问题：
            当要买主件i的时候才去判断主件i中的附件1、2能不能买和要不要买。
            1.只买主件
            2.主件+附件1，能不能买和要不要买
            3.主件+附件2，能不能买和要不要买
            4.主件+附件1+附件2，能不能买和要不要买
            最后，取这四种情景当中的最大值作为选购前i个主件的最优解。
        以下是具体代码实现。

 *ps：前i个物品装背包不等于前i个物品装进背包，也就是说f(n,i)表示的并不是背包装了i个物品而是前i个物品装背包的这个最优价值。
      同理，前i个商品选购的最大价值之和不等于购买前i个商品的最大价值之和，也就是也就是说f(n,i)表示的并不是购物单买了i个商品而是在这前i个商品中的最优选购方案得到的最大价值之和。
      而将“求前i个商品选购的最大价值之和”转换成“求前i-a个主件商品的最大价值之和”，在思维的理解上又会更巧妙和简单。
      所以中华文化博大精深，不同的语序和不同的文字表达都会影响理解，希望我这般阐述能够说明白。
 */
public class HJ16 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNext()) {
            String[] input = in.nextLine().split(" ");
            // 总预算 由于题目说商品价格都是10元的整数倍，这里可以把所有的价格和总预算都除于10以节省内存空间
            int budget = Integer.parseInt(input[0]) / 10;
            // 商品总件数
            int num = Integer.parseInt(input[1]);
            // 原始商品编号数组，用于映射原始输入的附件商品对应主件商品列表的下标
            int[] initGoodsData = new int[num+1];
            // 主件商品列表
            List<Goods> list = new ArrayList<>();
            // 需要在列表的0位下标填充一个默认商品，这样列表的长度就等于主件个数+1
            list.add(new Goods());

            // 对输入数据进行处理
            for (int i = 1; i <= num; i++) {
                String[] arr = in.nextLine().split(" ");
                int moeny = Integer.parseInt(arr[0]) / 10;
                int worth = Integer.parseInt(arr[1]) * Integer.parseInt(arr[0]);
                Goods goods = new Goods(moeny, worth);
                int q = Integer.parseInt(arr[2]);
                if (q == 0) {
                    list.add(goods);
                    initGoodsData[i] = list.size() - 1;
                } else {
                    int index = initGoodsData[q];
                    if (list.get(index).annex1 == null) {
                        list.get(index).annex1 = goods;
                    } else {
                        list.get(index).annex2 = goods;
                    }
                }
            }

            // 选购前n个主件的最大价值之和
            int mainNum = list.size() - 1;
            int[][] f = new int[budget+1][mainNum+1];

            // 动态规划算法:
            // 1.初始化
            for (int k = 0; k <= budget; k++) {
                f[k][0] = 0;
            }
            for (int i = 0; i <= mainNum; i++) {
                f[0][i] = 0;
            }
            // 2.自底向上迭代，求f(k,i)
            for (int k = 1; k <= budget; k++) {
                for (int i = 1; i <= mainNum; i++) {
                    f[k][i] = f[k][i-1];
                    if (list.get(i).moeny <= k) {
                        // 只选购主件
                        int temp = list.get(i).worth + f[k - list.get(i).moeny][i-1];
                        f[k][i] = Math.max(temp, f[k][i]);
                        // 选购主件+附件1
                        if (list.get(i).annex1 != null) {
                            if (list.get(i).moeny + list.get(i).annex1.moeny <= k) {
                                temp = list.get(i).worth + list.get(i).annex1.worth
                                        + f[k - list.get(i).moeny - list.get(i).annex1.moeny][i-1];
                                f[k][i] = Math.max(temp, f[k][i]);
                            }
                        }
                        // 选购主件+附件2
                        if (list.get(i).annex2 != null) {
                            if (list.get(i).moeny + list.get(i).annex2.moeny <= k) {
                                temp = list.get(i).worth + list.get(i).annex2.worth
                                        + f[k - list.get(i).moeny - list.get(i).annex2.moeny][i-1];
                                f[k][i] = Math.max(temp, f[k][i]);
                            }
                        }
                        // 选购主件+附件1+附件2
                        if (list.get(i).annex1 != null && list.get(i).annex2 != null ) {
                            if (list.get(i).moeny + list.get(i).annex1.moeny + list.get(i).annex2.moeny <= k) {
                                temp = list.get(i).worth + list.get(i).annex1.worth + list.get(i).annex2.worth
                                        + f[k - list.get(i).moeny - list.get(i).annex1.moeny - list.get(i).annex2.moeny][i-1];
                                f[k][i] = Math.max(temp, f[k][i]);
                            }
                        }
                    }
                }
            }

            // 结果输出
            System.out.println(f[budget][mainNum]);
        }
    }
}

class Goods {
    // 价格
    int moeny = 0;
    // 价值
    int worth = 0;
    // 附件1
    Goods annex1 = null;
    // 附件2
    Goods annex2 = null;

    public Goods() {
    }

    public Goods(int moeny, int worth) {
        this.moeny = moeny;
        this.worth = worth;
    }

}
