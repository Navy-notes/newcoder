package huawei;

import java.util.Scanner;

/**
 * @author Yuliang.Lee
 * @version 1.0
 * @date 2021/8/27 13:28
 * 识别有效的IP地址和掩码并进行分类统计：
 * 统计A、B、C、D、E、错误IP地址或错误掩码、私有IP的个数，之间以空格隔开。
   注意：
    1. 类似于【0.*.*.*】和【127.*.*.*】的IP地址不属于上述输入的任意一类，也不属于不合法ip地址，计数时可以忽略
    2. 私有IP地址和A,B,C,D,E类地址是不冲突的

 * 输入：
 * 10.70.44.68~255.254.255.0
 * 1.0.0.1~255.0.0.0
 * 192.168.0.2~255.255.255.0
 * 19..0.~255.255.255.0
 * 输出：
 * 1 0 1 0 0 2 1
 */
public class HJ18 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int countTypeA = 0;
        int countTypeB = 0;
        int countTypeC = 0;
        int countTypeD = 0;
        int countTypeE = 0;
        int countIllegal = 0;
        int countPrivate = 0;

        while (in.hasNextLine()) {
            String input = in.nextLine();
            if (input.length() == 0) {
                break;
            }
            String[] inputArr = input.split("~");
            String[] ipArr = inputArr[0].split("\\.");
            String[] maskArr = inputArr[1].split("\\.");
            // 先排除非法输入
            if (ipArr.length != 4 || maskArr.length != 4) {
                countIllegal++;
                continue;
            }
            // 再统计非法掩码
            if (!checkMask(maskArr)) {
                countIllegal++;
                continue;
            }
            // 分类统计IP（对不属于任何分类的IP判定为非法）
            String type = classifyIp(ipArr);
            switch (type) {
                case "A":
                    countTypeA++;
                    break;
                case "B":
                    countTypeB++;
                    break;
                case "C":
                    countTypeC++;
                    break;
                case "D":
                    countTypeD++;
                    break;
                case "E":
                    countTypeE++;
                    break;
                case "Illegal":
                    countIllegal++;
                    continue;
                default:
                    break;
            }
            // 最后统计私有IP（只有当IP合法时才统计）
            if (checkPrivateIp(ipArr)) {
                countPrivate++;
            }
        }
        System.out.println(countTypeA + " " + countTypeB + " " + countTypeC + " " + countTypeD + " "
                + countTypeE + " " + countIllegal + " " + countPrivate);

    }

    /**
     * 子网掩码为二进制下前面是连续的1，然后全是0。
     * （例如：255.255.255.32就是一个非法的掩码）
     * 注意二进制下全是1或者全是0均为非法
     */
    public static boolean checkMask(String[] arr) {
        int mask_1 = Integer.parseInt(arr[0]);
        int mask_2 = Integer.parseInt(arr[1]);
        int mask_3 = Integer.parseInt(arr[2]);
        int mask_4 = Integer.parseInt(arr[3]);
        // 掩码全为0或全为1的情况
        if (mask_1 == 0 || mask_4 == 255) {
            return false;
        }
        // 掩码二进制中，1之间存在0的情况
        StringBuilder sb = new StringBuilder(32);
        sb.append(String.format("%08d", Integer.parseInt(Integer.toBinaryString(mask_1))))
                .append(String.format("%08d", Integer.parseInt(Integer.toBinaryString(mask_2))))
                .append(String.format("%08d", Integer.parseInt(Integer.toBinaryString(mask_3))))
                .append(String.format("%08d", Integer.parseInt(Integer.toBinaryString(mask_4))));
        if (sb.toString().split("0").length > 1) {
            return false;
        }
        return true;
    }

    /**
     * 所有的IP地址划分为 A,B,C,D,E五类
     * A类地址1.0.0.0~126.255.255.255;
     * B类地址128.0.0.0~191.255.255.255;
     * C类地址192.0.0.0~223.255.255.255;
     * D类地址224.0.0.0~239.255.255.255;
     * E类地址240.0.0.0~255.255.255.255
     */
    public static String classifyIp(String[] arr) {
        int ip_1 = Integer.parseInt(arr[0]);
        int ip_2 = Integer.parseInt(arr[1]);
        int ip_3 = Integer.parseInt(arr[2]);
        int ip_4 = Integer.parseInt(arr[3]);
        if (ip_2 >= 0 && ip_2 <= 255
                && ip_3 >= 0 && ip_3 <= 255
                && ip_4 >= 0 && ip_4 <= 255) {
            if (ip_1 == 0 || ip_1 == 127) {
                return "Ignore";
            } else if (ip_1 >= 1 && ip_1 <= 126) {
                return "A";
            } else if (ip_1 >= 128 && ip_1 <= 191) {
                return "B";
            } else if (ip_1 >= 192 && ip_1 <= 223) {
                return "C";
            } else if (ip_1 >= 224 && ip_1 <= 239) {
                return "D";
            } else if (ip_1 >= 240 && ip_1 <= 255) {
                return "E";
            }
        }
        return "Illegal";
    }

    /**
     * 私网IP范围是：
     * 10.0.0.0～10.255.255.255
     * 172.16.0.0～172.31.255.255
     * 192.168.0.0～192.168.255.255
     */
    public static boolean checkPrivateIp(String[] arr) {
        int ip_1 = Integer.parseInt(arr[0]);
        int ip_2 = Integer.parseInt(arr[1]);
        if (ip_1 == 10 || (ip_1 == 172 && ip_2 >= 16 && ip_2 <= 31)
                || (ip_1 == 192 && ip_2 == 168)) {
            return true;
        }
        return false;
    }

}
