package huawei;

import java.util.Scanner;
import java.util.TreeMap;

/**
 * @author Yuliang.Lee
 * @version 1.0
 * @date 2021/8/20 11:01
 * 合并表记录：
   数据表记录包含表索引和数值（int范围的正整数），请对表索引相同的记录进行合并。
   即将相同索引的数值进行求和运算，输出按照key值升序进行输出。
 */
public class HJ8 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNextLine()) {
            int n = Integer.parseInt(in.nextLine());   // 键值对个数，即表记录的行数
            TreeMap<Integer, Integer> treeMap = new TreeMap<>();
            for (int i = 1; i <= n; i++) {
                String row = in.nextLine();
                String[] arr = row.split(" ");
                Integer key = Integer.parseInt(arr[0]);
                Integer value = Integer.parseInt(arr[1]);
                if (treeMap.containsKey(key)) {
                    treeMap.put(key, value + treeMap.get(key));
                }
                else {
                    treeMap.put(key, value);
                }
            }
            /**
             * 普通for循环： 当算法逻辑需要涉及到index下标时
             * foreach循环： 是基于Iterator迭代器实现的，但是无法使用迭代器的方法(Iterator.remove()等)，所以在遍历的时候不能增删集合元素。
                            属于三者中最简单高效的写法，当仅需要遍历集合且不涉及下标逻辑的时候，果断用它
             * iterator循环：可以在遍历集合的同时对集合进行增、删操作。https://blog.csdn.net/Mr_Mocha/article/details/105305481
             */
            for (Integer key: treeMap.keySet()) {
                System.out.println(key + " " + treeMap.get(key));
            }
        }
    }

}
