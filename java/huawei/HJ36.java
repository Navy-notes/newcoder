package huawei;

import java.util.LinkedHashSet;
import java.util.Scanner;
import java.util.Set;

/**
 * @author Yuliang.Lee
 * @version 1.0
 * @date 2021/9/15 12:35
 * 字符串加密：
    有一种技巧可以对数据进行加密，它使用一个单词作为它的密匙。下面是它的工作原理：首先，选择一个单词作为密匙，如TRAILBLAZERS。如果单词中包含有重复的字母，只保留第1个，其余几个丢弃。现在，修改过的那个单词属于字母表的下面，如下所示：
    A B C D E F G H I J K L M N O P Q R S T U V W X Y Z
    T R A I L B Z E S C D F G H J K M N O P Q U V W X Y
    上面其他用字母表中剩余的字母填充完整。在对信息进行加密时，信息中的每个字母被固定于顶上那行，并用下面那行的对应字母一一取代原文的字母(字母字符的大小写状态应该保留)。
    因此，使用这个密匙，Attack AT DAWN(黎明时攻击)就会被加密为Tpptad TP ITVH。
 * 示例：
    输入：
    nihao
    ni
    输出：
    le
 */
public class HJ36 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNext()) {
            String key = in.nextLine().toUpperCase();
            String str = in.nextLine();
            Set<Character> set = new LinkedHashSet<>();
            for (int i = 0; i < key.length(); i++) {
                set.add(key.charAt(i));
            }
            for (int ch = 65; ch < 91; ch++) {
                set.add((char) ch);
            }
            // 获得字符加密映射表
            char[] mapper = new char[26];
            int index = 0;
            for (Character e : set) {
                mapper[index] = e;
                index++;
            }
            // 加密输出
            for (int i = 0; i < str.length(); i++) {
                char ch = str.charAt(i);
                if (ch != ' ') {
                    if (ch - 65 > 26) {
                        // 小写
                        ch = (char) (mapper[ch - 97] + 32);
                    } else {
                        // 大写
                        ch = mapper[ch - 65];
                    }
                }
                System.out.print(ch);
            }
            System.out.println();
        }
    }
}
