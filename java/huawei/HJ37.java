package huawei;

import java.util.Scanner;

/**
 * @author Yuliang.Lee
 * @version 1.0
 * @date 2021/9/15 18:05
 * 统计每个月兔子的总数：
    有一只兔子，从出生后第3个月起每个月都生一只兔子，小兔子长到第三个月后每个月又生一只兔子，假如兔子都不死，问每个月的兔子总数为多少？
 * 解决问题的核心是建立数据结构和数学模型
    输入：9  (month)
    输出：34 (兔子总数)
 */
public class HJ37 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNext()) {
            // n代表第几个月
            int n = in.nextInt();
            // 用数组arr[1~3]代表第一个月时，三个笼子的兔子。
            int[] arr = {0, 1, 0, 0};
            // 从第二个月开始处理
            for (int i = 2; i <= n; i++) {
                // 其中第三个笼子的兔子不会死亡并且每个月会新生其同等数量的兔子
                int newborn = arr[3];
                // 第二个笼子的兔子每过一个月则迁移至第三个笼子并且同时新生兔子
                arr[3] += arr[2];
                newborn += arr[2];
                arr[2] = 0;
                // 第一个笼子的兔子每过一个月则迁移至第二个笼子
                arr[2] += arr[1];
                arr[1] = 0;
                // 新生的兔子加入到第一个笼子
                arr[1] += newborn;
            }
            // 最后所有笼子中的兔子相加即为总数
            System.out.println(arr[1] + arr[2] + arr[3]);
        }
    }
}




//public class HJ37 {
//    public static void main(String[] args) {
//        Scanner sc = new Scanner(System.in);
//        while (sc.hasNextInt()){
//            int month = sc.nextInt();
//            System.out.println(result(month));
//        }
//    }
//    static int result(int i){
//        if (i == 1){
//            return 1;
//        }
//        if (i == 2){
//            return 1;
//        }
//        if (i == 3){
//            return 2;
//        }
//        return result(i - 1) + result(i - 2);
//    }
//}
