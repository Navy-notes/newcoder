package huawei;

import java.util.Arrays;
import java.util.Scanner;

/**
 * @author Yuliang.Lee
 * @version 1.0
 * @date 2021/9/28 17:17
 * 名字的漂亮度：
    给出一个名字，该名字有26个字符组成，定义这个字符串的“漂亮度”是其所有字母“漂亮度”的总和。
    每个字母都有一个“漂亮度”，范围在1到26之间。没有任何两个不同字母拥有相同的“漂亮度”。字母忽略大小写。
    给出多个名字，计算每个名字最大可能的“漂亮度”。

 * 示例：
    输入：
        2
        zhangsan
        lisi
    输出：
        192
        101
 */
public class HJ45 {
    // 定义得分表
    public final static int[] score = {1, 2 , 3, 4 ,5 ,6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26};
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNext()) {
            int n = in.nextInt();
            while (n > 0) {
                // 由于漂亮度统计忽略大小写，所以统一转换为大写处理
                String name = in.next().toUpperCase();
                // 定义数组记录每个字母出现的个数，下标0代表A，1-B，2-C...，初始值默认都是0
                int[] lettersNum = new int[26];
                // 遍历给定的输入字符串
                for (int i = 0; i < name.length(); i++) {
                    lettersNum[name.charAt(i) - 65] += 1;
                }
                // 对数组进行排序（虽然这个时候下标所代表的字母发生变化了，但是不影响漂亮度的结果）
                Arrays.sort(lettersNum);
                // 计算漂亮度
                int result = 0;
                for (int i = 0; i < 26; i++) {
                    result += lettersNum[i] * score[i];
                }
                // 输出
                System.out.println(result);
                n--;
            }
        }
    }
}
