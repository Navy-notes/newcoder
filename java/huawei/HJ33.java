package huawei;

import java.util.Scanner;

/**
 * @author Yuliang.Lee
 * @version 1.0
 * @date 2021/9/9 12:34
 * 整数与IP地址之间相互转换
    原理：ip地址的每段可以看成是一个0-255的整数，把每段拆分成一个二进制形式组合起来，然后把这个二进制数转变成一个长整数。
        举例：一个ip地址为10.0.3.193
        每段数字             相对应的二进制数
        10                   00001010
        0                    00000000
        3                    00000011
        193                  11000001
        组合起来即为：00001010 00000000 00000011 11000001,转换为10进制数就是：167773121，即该IP地址转换后的数字就是它了。
 * 示例：
    输入：10.0.3.193
          167969729
    输出：167773121
          10.3.3.193
 */
public class HJ33 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNext()) {
            String ipAddr = in.next();
            long ipDeci = in.nextLong();
            System.out.println(transferToIpDecimal(ipAddr));
            System.out.println(transferToIpAddress(ipDeci));
        }
    }

    public static long transferToIpDecimal(String ipAddr) {
        String[] split = ipAddr.split("\\.");
        StringBuilder sb = new StringBuilder(32);   // 用于存储32位二进制数
        for (int i = 0; i < 4; i++) {
            sb.append(String.format("%08d", Integer.parseInt(Integer.toBinaryString(Integer.parseInt(split[i])))));
        }
        // 二进制转十进制
        return Long.parseLong(sb.toString(), 2);
    }


    public static String transferToIpAddress(long ipDeci) {
        String ipBinary = Long.toBinaryString(ipDeci);
        // 不足32位则需要补0
        if (ipBinary.length() < 32) {
            StringBuilder ipBinaryFill = new StringBuilder(32);
            for (int i = 1; i <= 32 - ipBinary.length(); i++) {
                ipBinaryFill.append('0');
            }
            ipBinaryFill.append(ipBinary);
            ipBinary = ipBinaryFill.toString();
        }
        StringBuilder ipAddr = new StringBuilder(16);   // 用于存储ip地址
        for (int i = 0; i < 32; i += 8) {
            ipAddr.append(Integer.parseInt(ipBinary.substring(i, i + 8), 2));
            ipAddr.append(".");
        }
        return ipAddr.substring(0, ipAddr.length() - 1);
    }
}
