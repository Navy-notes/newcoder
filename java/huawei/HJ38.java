package huawei;

import java.util.Scanner;



/**
 * @author Yuliang.Lee
 * @version 1.0
 * @date 2021/9/15 19:29
 * 求小球落地5次后所经历的路程和第五次反弹的高度：
    假设一个球从任意高度自由落下，每次落地后反跳回原高度的一半; 再落下, 求它在第5次落地时，共经历多少米?第5次反弹多高？
    最后的误差判断是小数点6位。
（偷懒的话可以直接用等比例模型即可）
 输入：1
 输出：2.875
      0.03125
 */
public class HJ38 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNext()) {
            int high = in.nextInt();
            double distance = 0;
            double rebound = high;
            for (int i = 1; i <= 5; i++) {
                // 第i次落地的路程
                distance += rebound;
                // 第i次反弹的高度
                rebound /= 2;
                // 第i次反弹的路程
                distance += rebound;
            }
            System.out.println(distance - rebound);
            System.out.println(rebound);
        }
    }
}
