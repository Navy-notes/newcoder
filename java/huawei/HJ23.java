package huawei;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

/**
 * @author Yuliang.Lee
 * @version 1.0
 * @date 2021/9/1 14:53
 * 删除字符串中出现最少的字符:
    实现删除字符串中出现次数最少的字符，若多个字符出现次数一样，则都删除。输出删除这些单词后的字符串，字符串中其它字符保持原来的顺序。
    注意每个输入文件有多组输入，即多个字符串用回车隔开。
    字符串只包含小写英文字母, 不考虑非法输入，输入的字符串长度小于等于20个字节。

 * 示例1：
   输入：
    abcdd
    aabcddd
   输出：
    dd
    aaddd
 *
 */
public class HJ23 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNext()) {
            String input = in.nextLine();
            List<Character> str = new LinkedList<>();
            Map<Character, Integer> countMap = new HashMap<>();
            for (int i = 0; i < input.length(); i++) {
                Character ch = input.charAt(i);
                str.add(ch);
                // 计数
                if (countMap.containsKey(ch)) {
                    countMap.put(ch, countMap.get(ch) + 1);
                } else {
                    countMap.put(ch, 1);
                }
            }
            List<Character> deleteChars = new ArrayList<>();
            int minApperance = input.length();
            // 找到出现次数最少的字符（有可能不止一种）
            for (Character key: countMap.keySet()) {
                int currentCount = countMap.get(key);
                if (currentCount < minApperance) {
                    minApperance = currentCount;
                    deleteChars.clear();
                    deleteChars.add(key);
                } else if (currentCount == minApperance) {
                    deleteChars.add(key);
                }
            }
            // 删除所有的出现次数最小的字符
            // ps：如果用str.remove('a')删除aabbb的话，会得到abbb，即对于重复的对象，只会移除出现的首个对象。
            str.removeAll(deleteChars);
            // 打印输出
            for (Character ch: str) {
                System.out.print(ch);
            }
            System.out.println();
        }
    }
}
