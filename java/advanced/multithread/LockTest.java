package advanced.multithread;

/**
 * sleep和wait的区别：
    sleep()是Thread类的静态方法，wait()是Object的非静态方法。
    sleep()会让线程处于休眠态，释放cpu资源但是不放弃锁；wait()使线程挂起阻塞，释放cpu和锁资源，需要等待notify()唤醒。
    sleep()方法可以在任何地方使用；wait()方法则只能在同步方法或同步块中使用
 */
public class LockTest {
    public static void main(String[] args) throws InterruptedException {
        A a = new A();

        long start = System.currentTimeMillis();

        // t1线程计数
        Thread t1 = new Thread(() -> {
            for (int i = 0; i < 10000000; i++ ) {
                a.increase();
//                System.out.println("t1:" + a.getNum());
//                try {
//                    Thread.sleep(100);      //会释放cpu资源但是不会释放锁
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
            }
        });
        t1.start();

        // 主线程计数
        for (int i = 0; i < 10000000; i++) {
            a.increase();
        }
        // 主线程需等待t1线程计数完成才能继续往下执行
        // 还可以用CountDownLatch的await()方法阻塞主线程，直到栅栏里的所有线程都执行完成
        t1.join();

        long end = System.currentTimeMillis();
        System.out.println(String.format("%sms", end - start));
        System.out.println(a.getNum());
    }
}
