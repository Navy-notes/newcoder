package advanced.multithread._threadlocal;

/**
 * InheritableThreadLocal的继承性：
     ThreadLocal类是不能提供子线程访问父线程的本地变量的，而InheritableThreadLocal可以。
     之前说到，每个线程中都有两个变量缓存threadLocals和inheritableThreadLocals。
     那么用ThreadLocal创建的实例会存放在每个调用线程各自的threadLocals里，threadLocals变量只对当前线程可见，对子线程是不可见的。
     而用InheritableThreadLocal创建的实例则存放在各调用线程缓存的inheritableThreadLocals中，对当前线程和子线程都可见。
 */
public class InheritableThreadLocalTest {
    // 创建ThreadLocal变量
    private static ThreadLocal<String> threadLocal = new ThreadLocal<>();
    // 创建InheritableThreadLocal变量
    private static InheritableThreadLocal<String> inheritableThreadLocal = new InheritableThreadLocal<>();

    public static void main(String[] args) {
        // 在main线程中添加main线程的本地变量
        threadLocal.set("mainVal");
        inheritableThreadLocal.set("inheritable");
        // 新创建一个子线程
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("子线程中的本地变量值: " + threadLocal.get() + "," + inheritableThreadLocal.get());
            }
        });
        thread.start();
        // 输出main线程中的本地变量值
        System.out.println("main线程中的本地变量值: " + threadLocal.get() + "," + inheritableThreadLocal.get());
    }
}
