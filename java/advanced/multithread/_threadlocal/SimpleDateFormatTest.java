package advanced.multithread._threadlocal;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 线程池和ThreadLocal类的简单使用：
 * ThreadLocal的实现原理：
    Thread类中有两个变量threadLocals和inheritableThreadLocals，其数据结构为ThreadLocalMap，也即HashMap。
    默认情况下，每个线程中的这两个变量都为null。只有当线程第一次调用ThreadLocal的set或者get方法的时候才会创建这两个变量。
    用ThreadLocal创建的实例会存放在每个调用线程各自的threadLocals里，我们称之为线程副本（根据JMM模型，每个线程都有自己独立的工作内存）。
    通过set方法将value添加到调用线程的threadLocals中，当调用线程调用get方法时候能够从它的threadLocals中取出value。
    如果调用线程一直不消亡，那么这个线程副本或者说是本地变量将会一直存放在调用线程的threadLocals中，所以不使用本地变量的时候需要调用ThreadLocal的remove方法释放线程缓存。

 * ThreadLocalMap容易造成内存泄漏的原因：
    ThreadLocalMap内部实际上是一个Entry数组（private Entry[] table），Entry中包含了属性key和vaule。
    其中key是ThreadLocal的实例对象，并传递给了弱引用WeakReference的构造函数。（当发生GC时，弱引用引用的对象无论内存是否足够都会被垃圾回收器清理掉）
    如果当前线程一直存在某副本变量但是这个ThreadLocal变量没有其他强依赖，当触发GC的时候，当前线程的ThreadLocalMap里面的ThreadLocal变量就会被回收，
    但是Entry由于仍被table数组的下标引用，而Entry中的属性value还引用着对应的Object对象，这个时候ThreadLocalMap会存在key为null但是value不为null的Entry项，从而造成内存泄漏。
    所以我们在实际使用完ThreadLocal对象后，要及时地调用remove方法避免内存泄漏。

 * 引用---强软弱虚：
     强引用：Object object=new Object()
     软引用：SoftReference类
     弱引用：WeakReference类
     虚引用：PhantomReference类
     一个对象只要有强引用存在就不会被GC。
     如果一个对象具有软引用，在JVM发生OOM之前（即内存充足够使用），是不会GC这个对象的；只有到JVM内存不足的时候才会GC掉这个对象。软引用和一个引用队列联合使用，如果软引用所引用的对象被回收之后，该引用就会加入到与之关联的引用队列中。
     被弱引用所引用的对象只能生存到下一次GC之前，当发生GC时候，无论当前内存是否足够，弱引用所引用的对象都会被回收掉。弱引用也是和一个引用队列联合使用，如果弱引用的对象被垃圾回收期回收掉，JVM会将这个引用加入到与之关联的引用队列中。若引用的对象可以通过弱引用的get方法得到，当引用的对象呗回收掉之后，再调用get方法就会返回null。
     虚引用是所有引用中最弱的一种引用，其存在就是为了将关联虚引用的对象在被GC掉之后收到一个通知，虚引用不能通过get方法获得其指向的对象。
 *
 */
public class SimpleDateFormatTest {
    // SimpleDateFormat在单例且多线程的情况下是非线程安全的
//    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    private static ThreadLocal<SimpleDateFormat> sdf = ThreadLocal.withInitial(() -> {
       System.err.println("new sdf");       //会被调用4次，即生成4个SimpleDateFormat实例
        /**
         * 在每个调用线程的ThreadLocalMap threadLocals中，key值都为sdf的引用，即ThreadLocal实例对象的地址；
         * value值则是线程中各自的SimpleDateFormat实例对象的地址。
         */
       return new SimpleDateFormat("yyyy-MM-dd");
    });

    public static void main(String[] args) {
        /**
         *  假设需要处理一个时间，将其格式化为字符串，而web程序会有很多用户同时访问。
         *  如果uv=1000，很简单地可以想到创建1000个线程去访问来解决并发的场景。
         *  但是实际上高峰期的最高并发只有130，创建1000个线程并发显然很浪费资源和性能。
         *  Tomcat实现的机制中用了线程池。
         *  线程池的使用方式：https://blog.csdn.net/achuo/article/details/80623893/
         */
        ExecutorService threadPool = Executors.newFixedThreadPool(4);
        threadPool.execute(() -> System.out.println(getFormat(new Date(2010 - 1900, 0, 13, 12, 23, 23 ))));
        threadPool.execute(() -> System.out.println(getFormat(new Date(2010 - 1900, 1, 13, 12, 23, 23 ))));
        threadPool.execute(() -> System.out.println(getFormat(new Date(2010 - 1900, 2, 13, 12, 23, 23 ))));
        threadPool.execute(() -> System.out.println(getFormat(new Date(2010 - 1900, 3, 13, 12, 23, 23 ))));
        threadPool.execute(() -> System.out.println(getFormat(new Date(2010 - 1900, 4, 13, 12, 23, 23 ))));
        threadPool.execute(() -> System.out.println(getFormat(new Date(2010 - 1900, 5, 13, 12, 23, 23 ))));
        threadPool.execute(() -> System.out.println(getFormat(new Date(2010 - 1900, 6, 13, 12, 23, 23 ))));
        threadPool.execute(() -> System.out.println(getFormat(new Date(2010 - 1900, 7, 13, 12, 23, 23 ))));
        threadPool.execute(() -> System.out.println(getFormat(new Date(2010 - 1900, 8, 13, 12, 23, 23 ))));
        threadPool.execute(() -> System.out.println(getFormat(new Date(2010 - 1900, 9, 13, 12, 23, 23 ))));
        threadPool.execute(() -> System.out.println(getFormat(new Date(2010 - 1900, 10, 13, 12, 23, 23 ))));
        threadPool.execute(() -> System.out.println(getFormat(new Date(2010 - 1900, 11, 13, 12, 23, 23 ))));


    }

    private static String getFormat(Date date) {
//        System.err.println("new sdf");        //会被调用12次，即生成12个SimpleDateFormat实例
        /**
         * 直接使用类变量，由于SimpleDateFormat不是线程安全的，多线程情况下会导致运行结果有误。
         */
//        return sdf.format(date);
        /**
         * 方式一：将SimpleDateFormat定义成局部变量，每来一次请求就new一个新的对象。
           这样做可以保证每个线程中数据的独立性从而保证线程安全，但是会非常耗费资源，性价比不高。
         */
//        return new SimpleDateFormat("yyyy-MM-dd").format(date);
        /**
         * 方式二：加锁使各线程串行执行。
           可以保证线程安全，但是性能差、费时间。
         */
//        synchronized (SimpleDateFormatTest.class) {
//            return sdf.format(date);
//        }
        /**
         * 方式三：使用ThreadLocal在不加锁的情况下保证数据的独立性从而保证操作上的线程安全。
         * 通常配合ThreadPoolExecutor来使用，线程副本其实也应用了线程池回收利用资源的思想。效率非常高，资源利用也比较合理。
         */
        return sdf.get().format(date);
    }
}
