package advanced.multithread._cas;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * 需求：
 *  模拟统计100个客户端访问网站的pv
 *
 * multi-thread-unsafe，非线程安全的实现
 */
public class Demo1 {
    // 总访问量
    static int count = 0;

    // 模拟访问请求
    public static void request() throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(5);
        /**
         * Q：分析一下问题出在哪呢？
         * A：count ++ 操作实际上是由3步来完成
         *      1.获取count的值，记做A： A = count
         *      2.将A值+1，得到B： B = A+1
         *      3.将B值赋值给count
         *
         *      如果有t1、t2两个线程同时执行count++，他们同时执行到上面步骤的第一步，
         *      得到的count是一样的。三步操作结束后，count最终止只加了1，导致结果不正确。
         */
        count++;
    }

    public static void main(String[] args) throws InterruptedException {
        // 开始时间
        long startTime = System.currentTimeMillis();
        // 线程个数
        int threadSize = 100;
        // 创建栅栏以确保所有线程完成，主线程才继续往下执行
        CountDownLatch countDownLatch = new CountDownLatch(threadSize);
        // 创建线程
        for (int i = 0; i < threadSize; i++) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    // 模拟用户行为，每个用户访问10次网站
                    try {
                        for (int j = 0; j < 10; j++) {
                            request();
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } finally {
                        countDownLatch.countDown();
                    }
                }
            }).start();
        }
        // 保证100个线程都结束之后，栅栏才放行
        countDownLatch.await();
        long endTime = System.currentTimeMillis();
        System.out.println(Thread.currentThread().getName() + ",耗时：" + (endTime - startTime) + ",count = " + count);
    }
}
