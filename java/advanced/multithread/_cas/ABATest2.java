package advanced.multithread._cas;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicStampedReference;

/**
 * 采用JDK提供的方案解决ABA问题：
    加入版本号控制，每次值变化，都会修改它的版本号，并且CAS更新值之前还得去比对版本号是否符合预期。
    AtomicStampedReference<T>类的方法compareAndSet(期望引用, 新值引用, 期望引用的版本号, 新值引用的版本号)
 */
public class ABATest2 {
    private static AtomicStampedReference<Integer> a = new AtomicStampedReference<>(new Integer(1), 1);

    public static void main(String[] args) {
        Thread main = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println(Thread.currentThread().getName() + ", 初始值：" + a.getReference());
                Integer expectReference = a.getReference();     // 做更新操作前期望的值是多少,---1
                Integer newReference = expectReference + 1;     // 要把值更新为多少,---2
                int expectStamp = a.getStamp();                 // 做更新操作前期望的版本号是多少,---1
                int newStamp = expectStamp + 1;                 // 要把版本号更新为多少,---2
                try {
                    // 主线程休眠一秒钟，让出cpu
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                boolean isCASSuccess = a.compareAndSet(expectReference, newReference, expectStamp, newStamp);
                System.out.println(Thread.currentThread().getName() + ", CAS操作：" + isCASSuccess);
            }
        }, "主线程");

        Thread other = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    // 确保Thread-main线程优先执行
                    Thread.sleep(20);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                // a + 1 --->  a = 2, stamp = 2
                Integer reference = a.getReference();
                int stamp = a.getStamp();
                a.compareAndSet(reference, reference + 1, stamp, stamp + 1);
                System.out.println(Thread.currentThread().getName() + ", 【increment】,值=" + a.getReference());
                // a - 1 --->  a = 1, stamp = 3
                reference = a.getReference();
                stamp = a.getStamp();
                a.compareAndSet(reference, reference - 1, stamp, stamp + 1);
                System.out.println(Thread.currentThread().getName() + ", 【decrement】,值=" + a.getReference());
            }
        }, "干扰线程");

        main.start();
        other.start();
    }

}
