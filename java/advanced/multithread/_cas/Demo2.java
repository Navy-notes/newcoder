package advanced.multithread._cas;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * 需求：
 *  模拟统计100个客户端访问网站的pv
 *
 * 锁机制，对request方法用关键字synchronized修饰。
   可以保证线程安全，但耗时太久，性能差。
 */
public class Demo2 {
    // 总访问量
    static int count = 0;

    // 模拟访问请求
    public static synchronized void request() throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(5);
        /**
         * 使用synchronized或ReentranLock保证线程并发操作时，request方法相当于被"串行"执行。
         */
        count++;
    }

    public static void main(String[] args) throws InterruptedException {
        // 开始时间
        long startTime = System.currentTimeMillis();
        // 线程个数
        int threadSize = 100;
        // 创建栅栏以确保所有线程完成，主线程才继续往下执行
        CountDownLatch countDownLatch = new CountDownLatch(threadSize);
        // 创建线程
        for (int i = 0; i < threadSize; i++) {

            new Thread(new Runnable() {
                @Override
                public void run() {
                    // 模拟用户行为，每个用户访问10次网站
                    try {
                        for (int j = 0; j < 10; j++) {
                            request();
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } finally {
                        countDownLatch.countDown();
                    }
                }
            }).start();
        }
        // 保证100个线程都结束之后，栅栏才放行
        countDownLatch.await();
        long endTime = System.currentTimeMillis();
        System.out.println(Thread.currentThread().getName() + ",耗时：" + (endTime - startTime) + ",count = " + count);
    }
}
