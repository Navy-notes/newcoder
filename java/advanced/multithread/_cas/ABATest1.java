package advanced.multithread._cas;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * CAS在操作值的时候会先检查值有没有发生变化，只有当预期值没有发生变化时才更新，否则会放弃锁重新进入自旋等待。
   但是如果一个值原来是a，在CAS方法执行之前，被其他线程改为了b，然后又修改回了a，那么CAS方法有可能会在执行检查的时候发现
   它的值没有发生变化而更新它，但实际预期值是变化了的，这就是CAS的ABA问题。
 *
 * 以下使用程序模拟ABA现场。
 */
public class ABATest1 {
    private static AtomicInteger a = new AtomicInteger(1);

    public static void main(String[] args) {
        Thread main = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println(Thread.currentThread().getName() + ", 初始值：" + a.get());
                int expectNum = a.get();
                int updateNum = expectNum + 1;
                try {
                    // 主线程休眠一秒钟，让出cpu
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                boolean isCASSuccess = a.compareAndSet(expectNum, updateNum);   // 模拟incrementAndGet()的源码实现
                System.out.println(Thread.currentThread().getName() + ", CAS操作：" + isCASSuccess);
            }
        }, "主线程");

        Thread other = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    // 确保Thread-main线程优先执行
                    Thread.sleep(20);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                a.incrementAndGet();        // a+1,a=2
                System.out.println(Thread.currentThread().getName() + ", 【increment】,值=" + a.get());
                a.decrementAndGet();        // a-1,a=1
                System.out.println(Thread.currentThread().getName() + ", 【decrement】,值=" + a.get());
            }
        }, "干扰线程");

        main.start();
        other.start();
    }

}
