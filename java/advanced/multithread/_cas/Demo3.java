package advanced.multithread._cas;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * 需求：
 *  模拟统计100个客户端访问网站的pv

 *  CAS的核心是compareAndSwap方法，实际上是一种自旋机制（轻量级锁、乐观锁），近似于无锁的并发编程。
    CAS编程层面上看是无锁的，效率很高，但CAS是线程安全的吗，如果是那么如何保证原子性？
        compareAndSet实际是通过JNI调用C++实现的本地方法，然后在汇编层面会判断操作系统是否为多核处理器，如果是多核处理，则会在cmpxchgq指令前加上lock。
        所以我们常说的CAS无锁编程，实际上在硬件指令级别还是会加锁（缓存行锁或总线锁），来保证线程安全。
        如果数据占用的内存小，直接通过缓存行锁即可（默认）；如果占用内存比较大，则需要用到总线锁。
    既然sychronized和CAS实际上都是加锁来保证原子性的，为什么说CAS效率很高，近乎无锁编程？
        1)首先CAS加锁的粒度范围更小。关于CAS机制JDK给出的实现是atomic包，也可以称之为原子量操作，JMM模型中涉及到的原子操作有
        read、load、use、store、write、assign、lock。锁的粒度越小，CPU并发的能力越强效率越高。
        2)还有一个很重要的原因就是，当线程获取不到重量级锁的时候，会被阻塞挂起，线程从用户态进入内核态，需要保存工作现场的上下文并切换，开销非常大。
        好比开汽车不断启停一样耗费油量更大。而CAS是轻量级锁，线程没有获取到锁后并不会被阻塞，而是保持占有CPU自旋等待，线程一直处在用户态不需要切换上下文。
        虽然每个线程占用CPU的时间更长了，但是在高并发且每个线程占用锁资源不长的情况下，这效率会非常高。

 *  CAS机制的使用前提是，需要volatile修饰多线程操作的变量，保证所有线程对变量的最新值的可见性。
    即每次操作该变量前都不用自己线程中的缓存而是去主内存中拉取一下最新值。

 *  CAS机制和锁机制各有优劣，并不是说CAS就一定好，都有各自应用的场景：
      当写操作很快，线程数较多时，尽可能采用CAS编程来控制线程安全；而当写逻辑比较复杂，线程数较少时，可能采用锁机制保证同步会更好。

 *  CAS的缺点：
    1、性能方面-高并发场景下，如果同步的逻辑复杂耗时久，那么一直让多个线程保持自旋空转会消耗大量的cpu资源。
    2、数据方面-ABA问题：
         如果业务要求ABA非常敏感，数据准确性非常严格，（eg银行业务），那么不建议使用CAS机制，而宁愿牺牲性能采用重量级的锁机制。
         如果业务对ABA并不十分敏感，但是要求尽可能的支持高并发，可采用JDK提供的解决方案：
             思路---加入版本号控制。每次值变化，都会修改它的版本号，CAS操作成功的前提还得去对比版本号。
             实现---AtomicStampedReference<T>类的方法compareAndSet(期望引用, 新值引用, 期望引用的版本号, 新值引用的版本号)可修复ABA的bug。
 */
public class Demo3 {
    // 总访问量
    // 严谨的话得用volatile修饰，但其实这里不用volatile也能保证count的可见性，原理详见advanced.multithread._synchronized.VisiblityTest
    static volatile int count = 0;

    // 模拟访问请求
    public static void request() throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(5);
        /**
         * Q：耗时太长的原因是什么？
         * A： 使用synchronized或ReentranLock保证线程并发操作时，多线程被"串行"执行。
         *     request方法同一时刻只允许一个线程进入，并发程度低，方法粒度的锁操作开销太大。
         *
         * Q：如何解决耗时长的问题？
         * A：减小同步阻塞的粒度，只在关键的操作步骤采用自旋机制来获取资源（近似于无锁编程）。
         *    count++的操作步骤：
         *      1.获取count的值，记做A： A = count
         *      2.将A值+1，得到B： B = A+1
         *      3.将B值赋值给count
         *    实际上，我们只需要对第三步操作保证原子性即可：
         *      3_1.获取锁
         *      3_2.获取count的最新值(需要让count对所有线程可见)，记作LV
         *      3_3.判断LV是否等于A，如果相等，则将B的值赋值给count，并返回true，否则返回false
         *      3_4.释放锁
         */
        int expectValue;
        while (!compareAndSwap((expectValue = getCount()), expectValue + 1)){}
    }

    // 模拟Atomic包的源码中，对compareAndSet的实现：
//    public static void compareAndSet(int expectValue, int newValue) {
//        while (!compareAndSwap(expectValue, newValue)){
//        }
//    }


    /**
     * @param expectValue 期望值，即A
     * @param newValue 需要给count赋值的新值，即B
     * @return
     */
    public static synchronized boolean compareAndSwap(int expectValue, int newValue) {
        if (expectValue == getCount()) {
            count = newValue;
            return true;
        }
        return false;
    }

    /**
     * 从主内存而非线程缓存中获取的当前最新的count
     * @return
     */
    public static int getCount() {
        return count;
    }

    public static void main(String[] args) throws InterruptedException {
        // 开始时间
        long startTime = System.currentTimeMillis();
        // 线程个数
        int threadSize = 100;
        // 创建栅栏以确保所有线程完成，主线程才继续往下执行
        CountDownLatch countDownLatch = new CountDownLatch(threadSize);
        // 创建线程
        for (int i = 0; i < threadSize; i++) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    // 模拟用户行为，每个用户访问10次网站
                    try {
                        for (int j = 0; j < 10; j++) {
                            request();
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } finally {
                        countDownLatch.countDown();
                    }
                }
            }).start();
        }
        // 保证100个线程都结束之后，栅栏才放行
        countDownLatch.await();
        long endTime = System.currentTimeMillis();
        System.out.println(Thread.currentThread().getName()+ ",耗时：" + (endTime - startTime) + ",count = " + count);
    }
}
