package advanced.multithread._aqs;

import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 使用Semaphore模拟控制商场厕所的并发场景
 * 用信号量共享资源的输出结果------100个线程分10批次使用资源，每批10个，基本上每秒1批。
 *
 * Semaphore实现原理：
    Semaphore也是AQS的一种实现，用来保护一个或者多个共享资源的访问。Semaphore内部维护了一个计数器，其值等于可以访问的共享资源的个数。
    一个线程要访问共享资源，先获得信号量，如果信号量的计数器值大于1，意味着有共享资源可以访问，则使其计数器值减去1，再访问共享资源。
    如果计数器值为0，则线程入队阻塞。当某个线程使用完共享资源后，释放信号量，并将信号量内部的计数器加1，之前进入休眠的线程将被唤醒并再次试图获得信号量。

 * 锁可以理解为信号量是1的资源，Semaphore除了控制资源的多个副本的并发访问，也可以使用二进制信号量(state)来实现类似synchronized关键字和Lock锁的并发访问控制功能。
 */
public class SemaphoreTest {
    static class ResourceManage {
        private final Semaphore semaphore;
        private final ReentrantLock lock;
        private int size = 10;
        private boolean resourceArray[];

        public ResourceManage() {
            // 存放厕所状态
            this.resourceArray = new boolean[size];
            // 控制10个共享资源的使用，使用先进先出的公平模式进行共享;公平模式的信号量，先来的先获得信号量
            this.semaphore = new Semaphore(size, true);
            // 公平模式的锁，先来的先选
            this.lock = new ReentrantLock(true);
            // 初始化为资源可用的情况
            for (int i = 0; i < size; i++) {
                resourceArray[i] = true;
            }
        }

        public void useResource(String username) throws InterruptedException {
            int id = -1;
            try {
                // 获取信号量
                semaphore.acquire();
                // 占到一个坑
                id = getResourceId();
//                System.out.print(username + "正在使用资源，资源id:" + id + "\n");
                // 蹲坑中...，相当于使用资源
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                // 退出这个坑
                resourceArray[id] = true;
                // 释放信号量，计数器加1
                semaphore.release();
            }
        }

        private int getResourceId() {
            int id = -1;
            try {
                // 这里使用ReentranLock（也可以使用AtomicBoolean类）保证了资源获取时的原子性，避免有多个人同时蹲到一个坑。
                lock.lock();
                for (int i = 0; i < size; i++) {
                    if (resourceArray[i]) {
                        resourceArray[i] = false;
                        id = i;
                        break;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }
            return id;
        }
    }

    public static void main(String[] args) {
        ResourceManage resourceManage = new ResourceManage();
        // 创建多个线程，模拟资源使用者
        Thread[] users = new Thread[100];

        for (int i = 0; i < 100; i++) {
            users[i] = new Thread(() -> {
                String username = Thread.currentThread().getName();
                try {
                    resourceManage.useResource(username);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.print(username + "使用资源完毕...\n");
            }, "user" + i);
        }

        for(int i = 0; i < 100; i++){
            Thread thread = users[i];
            // 启动线程
            thread.start();
        }
    }

}
