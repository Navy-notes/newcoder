package advanced.multithread._aqs;

import java.util.concurrent.locks.ReentrantLock;

/**
 * AQS-六个操作：
     volatile int state：
        默认是0，表示没有线程占有锁；改成1，则表示有线程占有了锁。
     CLH双向队列：
        和普通队列不同的是，CLH队列首次初始化会创建两个Node节点，第一个节点(head)可以看成是获取到锁的正在执行的线程，第二个节点开始才是等待锁资源的线程。

     【抢占锁】
         1.看锁标志位，默认是0，所以这时候会有线程抢到锁
            抢到锁以后需要改锁标志位---标志位state需要实现可见性
            需要一种机制保证原子性---CAS
         2.如果已经有线程占有了锁，而这时又有线程来抢锁
             a.当前来抢锁的线程是不是占有锁的线程？
                是->重入->state+1
                为什么要记录次数？
                    因为释放锁的时候需要释放对应的次数，让state回归零状态。
             b.如果不是，抢锁失败。
         3.优化：看等待区有没有人，如果有人，锁肯定被占用了。
            如何判断有人在等待？
                hasQueuedPredecessors

            公平锁和非公平锁的区别就在于获取锁的方式是必须要排队还是说可以(插队)抢占的，如果是必须排队获取的则是公平锁，否则为非公平锁。
                fair：
                     if (!hasQueuedPredecessors() &&
                         compareAndSetState(0, acquires)) {
                         setExclusiveOwnerThread(current);
                         return true;
                     }
                nonFair：
                     if (compareAndSetState(0, acquires)) {
                         setExclusiveOwnerThread(current);
                         return true;
                     }
    【入队】
        addWaiter(Node mode)
        来第一个等待的线程则初始化CLH队列（两个节点），后面排队等待的线程则从尾部插入。
        入队完以后做什么？
            队列中的第二个节点再次尝试获取锁acquireQueued(node, arg)，若失败则设置前驱节点的waitStatus为-1然后进入阻塞状态；
            其他（不是第二个的）节点进来则设置闹钟(waitStatus)、然后阻塞。
    【阻塞】
        LockSupport.park(this);
    【释放锁】
        tryRelease(arg);
         1.可重入问题
         2.状态位恢复为0
         3.唤醒队列中等待的线程
    【唤醒】
        unparkSuccessor(h);
    【出队】
        正常情况：
            出队这件事情是由被唤醒的第一个排队的线程(第二个节点)来做的。
            acquireQueued(final Node node, int arg) {
                ...
                for(;;){
                     final Node p = node.predecessor();
                     if (p == head && tryAcquire(arg)) {
                         setHead(node);
                         p.next = null; // help GC
                         failed = false;
                         return interrupted;
                     }
                    ...
                }
                ...
            }
        特殊情况：
            第二个节点唤醒失败，则从队列的中部出队，然后head节点(当前执行的线程)会唤醒下一个排队的线程(即第三个节点)。


 * ReentranLock 和 synchronzed 的区别：
     ReentranLock支持公平和非公平调度（默认是非公平），而synchronized是非公平锁。
     ReentranLock支持以非阻塞方式tryLock获取锁，并返回是否加锁成功的结果，可以很好地解决死锁问题。 （例：advanced.multithread._synchronized.deadlock.Test3）
     ReentranLock在编程上灵活度更高，可以（在被阻塞了之后）响应中断，可以设置限时，还可以更好的设置加锁的前置条件等。
     ReentranLock使用上没有sychronized简便，需要在finally块手动释放锁，处理异常等；synchronized会由JVM自动释放锁。
     ReentranLock是用纯Java实现的，synchronized底层是用C++实现的。
     ReentranLock和synchronized都支持可重入，当某个线程已经获取到锁以后，可以再次重复获取此锁。可重入锁阻塞其他线程不阻塞自己。
     ReentranLock和synchronzed都是重量级锁、悲观锁，都具有原子性和可见性，底层实现时都有用到CAS机制来做一些细节上的优化。
 *
 */
public class ReentranLockTest {
    private static int m = 0;
    private static ReentrantLock lock = new ReentrantLock();    // 默认是非公平锁，设置公平锁则需new ReentranLock(true);

    public static void main(String[] args) throws InterruptedException {
        int threadNum = 3;
        Thread[] threads = new Thread[threadNum];
        for (int i = 0; i < threadNum; i++) {
            threads[i] = new Thread(() -> count(), "t" + i);
            threads[i].start();
        }
        for (Thread thread : threads) {
            // 这段代码的位置不能放在17行的后面，这样会阻塞主线程第15行的for循环，导致t0、t1、t2实际上是顺序串行执行的，相当于没有并发。
            // 这里虽然也会阻塞主线程第19行的for循环，但此时三个子线程都已经在并发运行中，而这里的循环保证了所有子线程都运行完毕以后再执行主线程后面的syso逻辑。
            thread.join();

            //引申：
            // Thread.yield()是让当前运行线程回到可运行状态，以允许具有相同优先级的其他线程获得运行机会。
            // 因此，使用yield()的目的是让相同优先级的线程之间能适当的轮转执行。
            // 但是，实际中无法保证yield()达到让步目的，因为让步的线程还有可能被线程调度程序再次选中。
            // 不一定会阻塞当前线程，有点类似Thread.sleep(0)，只不过Thread.sleep会重新评估所有线程的优先级然后再抢占，yield是优先让同等级的线程们抢占。
        }
        System.out.println("m = " + m);
    }

    private static void count() {
        try {
            lock.lock();
            for (int i = 0; i < 10000; i++) {
                m++;
            }
//            System.out.println(Thread.currentThread().getName() + " finish!");
        } finally {
            lock.unlock();
        }
    }
}
