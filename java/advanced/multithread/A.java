package advanced.multithread;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * CAS-无锁原子操作(乐观锁)
 */
public class A {
    int num = 0;

//    public synchronized void increase() {
//        // 这里锁的是对象实例，相当于synchronized(this){...}
//        num++;
//    }

    // 使用原子操作替代锁，优化性能
    AtomicInteger atomicInteger = new AtomicInteger();
    public void increase() {
        atomicInteger.incrementAndGet();
    }

    public long getNum() {
//        return num;
        return atomicInteger.get();
    }
}
