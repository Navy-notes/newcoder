package advanced.multithread._volatile;

/**
 * 【volatile的有序性】
 * 双重检测锁DCL的对象半初始化问题：
   重排序可能使对象在半初始化的状态就被访问了，这在单线程中不影响结果但是在多线程中将会非常致命，虽然发生概率极低，只有在非常高的并发情况下才有可能发生。
   解决办法是在类变量instance前使用volatile关键字修饰，避免在创建DoubleCheckLockSingleton时发生重排序。

 * 一个Java对象的创建和初始化过程：（先父再子，先类资源再实例，先属性、方法、代码块最后才是用户实现的构造方法）
    父类静态资源 -> 子类静态资源 -> 父类实例属性、方法、代码块 -> 父类构造方法 -> 子类实例属性、方法、代码块 -> 子类构造方法
    newcoder\java\advanced\jvm\InitProcessTest.java
 * 半初始化：即给对象在堆分配了内存空间，创建了默认的属性和方法，但是还没有完成初始化init(程序员指定的初始赋值)


 * 指令重排序：在不影响(单线程)程序执行结果的前提下，计算机为了最大限度的发挥机器性能，会对机器指令重排序优化。
        as-if-serial原则：不管怎么重排序，单线程程序的执行结果不能被改变。所以编译器不会对存在数据依赖关系的操作做重排序。
        happens-before原则：使用锁机制、volatile等可以避免代码重排序。
 * 内存屏障：
        在两条指令之间加入一行屏障代码，使得这个屏障的上下两行指令代码无法被重排序，称之为内存屏障。
 * volatile保证有序性的原理正是因为实现了内存屏障：
        volatile在JVM底层使用StoreStore、LoadLoad、StoreLoad、LoadStore等规定的屏障指令；
        这些JVM屏障指令在汇编层会被编译成lock指令，lock指令规定其前后的指令不能进行重排序；
        lock指令在不同的CPU中又会被解释成相应的屏障机器指令来执行。
 */
public class DoubleCheckLockSingleton {
    private static DoubleCheckLockSingleton instance = null;

    private DoubleCheckLockSingleton() {

    }

    public static DoubleCheckLockSingleton getInstance() {
        // 第一重检测
        if (instance == null) {
            // 加锁
            synchronized (DoubleCheckLockSingleton.class) {
                // 第二重检测
                if (instance == null) {
                    instance = new DoubleCheckLockSingleton();
                }
            }
        }
        return instance;
    }


    public static void main(String[] args) {
        // 如果不加锁机制控制，那么当有多个线程同时创建第一个实例对象时，无法保证单例模式。
        DoubleCheckLockSingleton dclInstance = DoubleCheckLockSingleton.getInstance();
    }
}
