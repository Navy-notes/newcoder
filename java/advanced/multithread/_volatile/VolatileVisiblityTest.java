package advanced.multithread._volatile;

/**
 * 【volatile可见性】
 *  主内存即系统内存，工作内存即线程缓存。
 *  volatile修饰的变量，在更新值后，会马上刷写回主内存(store + write)并通过总线的嗅探机制被其他线程感知，然后失效自己工作内存中该变量的数据，
    等待需要读该变量，再从主内存里重新加载(read + load)，保证变量及时的可见性(立刻可见)。

 *  volatile缓存可见性和有序性的实现原理：
         1)会锁定系统内存的变量（缓存行锁定），然后将当前处理器缓存的数据立即写回到系统内存。
         2)这个写回内存的操作会引起在其他CPU里缓存了该内存地址的数据无效（MESI协议）。
         3)提供内存屏障功能，通过lock指令使其前后的操作不能重排序。

 *  注：volatile可以修饰类变量和(全局)实例变量，但是不可以修饰局部变量、类和方法。
        volatile保证可见性和有序性，但是不保证原子性。（所以需要与锁机制配合使用来保证线程安全）
 *
 */
public class VolatileVisiblityTest {
    private static volatile boolean initFlag = false;

    public static void main(String[] args) throws InterruptedException {
        new Thread(() -> {
            System.out.println("waiting data...");
            while (!initFlag) {

            }
            System.out.println("==============success");
        }).start();

        Thread.sleep(2000);
        new Thread(() -> prepareData()).start();
    }

    public static void prepareData() {
        System.out.println("prepare data...");
        initFlag = true;
        System.out.println("prepare data end...");
    }
}
