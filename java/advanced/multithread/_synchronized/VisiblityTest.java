package advanced.multithread._synchronized;

import java.util.concurrent.locks.ReentrantLock;

/**
 * 【synchronized的可见性】
 * JMM关于synchronized的两条规定：
    1）线程解锁前，必须把共享变量的最新值刷新到主内存中。
 　 2）线程加锁时，将清空工作内存中共享变量的值，从而使用共享变量时需要从主内存中重新获取最新的值。
 　　　（注意：加锁与解锁需要是同一把锁）
    通过以上两点，可以看到synchronized能够实现可见性。同时，由于synchronized具有同步锁，所以它也具有原子性。

 * 但是可以发现，synchronized的可见性其实对程序员/用户来说可操作性并不高，用volatile在编程上会更容易理解和保证程序的准确性。
 */
public class VisiblityTest {
    static class Marker{
        /**
         * 当把第58行代码注释掉以后，程序会进入死循环无法结束，原因是多线程之间失去了对b的可见性。
         * 这时采用volatile关键字的方式也能保证可见性，但是把关键字加到属性b前和加到marker属性前的效果完全不一样。
         * 当用volatile修饰"public boolean b;"时，程序可以正常结束，而当使用"volatile Marker marker;"时，程序依然无法结束。
         * 原因是当线程修改marker中的b属性时，marker的引用值未发生改变，而是它引用的对象的属性值b改变了，
         * "volatile Marker marker"实现的是marker对象对多线程的可见性而不是属性b的可见性，所以主线程自然无法感知到b的新值。
         */
        public boolean b;
        public Marker(boolean b){
            this.b = b;
        }
    }

    static class MyThread implements  Runnable{
        Marker marker;
        public MyThread(Marker marker){
            this.marker = marker;
        }
        public void run() {
            try{
                Thread.sleep(10);
            }catch (Exception e){
                System.out.println(e.toString());
            }
            System.out.println("The marker.b is set false.");
            this.marker.b = false;
        }
    }

    public static void main(String[] args) {
        Marker marker = new Marker(true);
        ReentrantLock lock = new ReentrantLock();

        new Thread(new MyThread(marker)).start();

        while(marker.b) {
            /**
             * 这个程序可以自行停下的原因是println的源码实现中用到了synchronized关键字：
               public void println(String x) {
                synchronized (this) {
                    print(x);
                    newLine();
                }
               }
             * 而synchronized除了具备原子性外，还具有可见性，所以主线程能感知到maker的b属性已经被修改为false了。
             */
//            System.out.println("ok");

            /**
             * 可以验证 ReentrantLock 也是具有可见性的。
             */
//            lock.lock();
//            lock.unlock();
        }
    }
}
