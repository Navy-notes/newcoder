package advanced.multithread._synchronized;

/**
 * 【synchronized的原子性】 （互斥锁、悲观锁、同步锁、重量级锁）
 *  synchronized的三种锁状态：偏向锁 ---------------------------> 轻量级锁(一直自旋) ------------------------------------> 重量级锁（自适应自旋）
                                    [多个线程加锁、CAS轻度竞争]                      [CAS自旋(默认10次)不成功，锁膨胀，进入重度竞争]
    重量级锁会导致线程挂起阻塞、上下文保存和切换、操作系统线程调度等一系列工作；自旋锁会不断的重试抢占锁资源，并不会挂起阻塞使用户线程进入内核态。
    值得一提的是，jdk1.6以前sychronized直接就是重量级锁，并没有以上三种锁状态的过渡，导致效率不高。所以从1.6开始才有了sychronized上述的一些细节上的优化。

 *  可以作为(静态或非静态)方法的关键字修饰或者使用代码块synchronized(this/xxx.class){...}
    同步代码块 可以等价于 同步方法，这两种方式并没有本质的区别，只是写法和用法上不同而已。synchronized最大的区别应该是对象锁和类锁的分别。

 *  理解锁机制的时候，要把锁看成是一个资源来理解。同步逻辑执行的前提是，获得锁资源：
       类锁和对象锁可以理解成是两个不同的资源。对象锁是标记在对象头信息中，每个实例对象有且只有一个对象锁，并且不同对象实例的对象锁相互独立互不干扰。
       类锁实际并不存在，更多的是逻辑概念，用来帮助我们理解锁机制在实例方法和静态方法上的区别，每个类只有一个类锁，并且由这个类的所有实例对象共同拥有，所以类锁的影响范围是类的所有对象实例。
    注意：不需锁资源的逻辑操作不受对象锁和类锁阻塞，对象锁和类锁两者的同步逻辑是相互独立的并不会产生阻塞。只有对象锁和对象锁之间，类锁和类锁之间的逻辑操作才会有同步竞争。
 */
public class SyncThread extends Thread {
    MyTest myTest;

    public SyncThread(MyTest myTest, String name) {
        super(name);
        this.myTest = myTest;
    }


    @Override
    public void run() {
        synchronized (MyTest.class) {
            myTest.countNum();
        }
    }


    public static void main(String[] args) {
        MyTest myTest1 = new MyTest();
        MyTest myTest2 = new MyTest();
        SyncThread thread1 = new SyncThread(myTest1, "t1");
        thread1.start();
        new Thread(() -> {
            System.out.println(Thread.currentThread().getName() + ": " + myTest2.getNum());
        }, "t2").start();
    }


    static class MyTest {
        private static int num = 0;

        public void countNum() {
            for (int i = 0; i < 1000000000; i++) {
                num++;
            }
        }

        public static synchronized int getNum() {
            return num;
        }
    }

}
