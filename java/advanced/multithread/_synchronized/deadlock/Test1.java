package advanced.multithread._synchronized.deadlock;

/**
 * sychronized进入阻塞状态不会释放锁，重量级锁是利用了操作系统的Mutex量来实现的，机制都是占有且等待。
 * 线程进入内核态并不意味着会释放锁，两者没有必然联系。
 * 凡是涉及到操作系统才能完成的操作都需要线程进入到内核态，比如读取磁盘，发送网络数据，线程调度，上下文切换等等。
 * 而线程执行用户编写的代码和逻辑时则是处在用户态。
 * 所以线程休眠sleep、挂起wait、阻塞lock都会从用户态进入内核态。
 */
public class Test1 {
    public static void main(String[] args) throws InterruptedException {
        Account account1 = new Account(1000);
        Account account2 = new Account(1000);

        MyThread myThread1 = new MyThread(account1, account2, 100);
        MyThread myThread2 = new MyThread(account2, account1, 100);
        myThread1.start();
        myThread2.start();
        myThread1.join();
        myThread2.join();
        System.out.println("账户1的余额：" + account1.getMoney() + "账户2的余额:" + account2.getMoney());
    }

    static class MyThread extends Thread {
        private final Account account1;
        private final Account account2;
        private final int num;

        public MyThread(Account account1, Account account2, int num) {
            this.account1 = account1;
            this.account2 = account2;
            this.num = num;
        }

        @Override
        public void run() {
            synchronized (account1) {
                try {
                    sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                synchronized (account2) {
                    if(account1.subMoney(num)) {
                        account2.addMoney(num);
                    }
                }
            }
        }
    }

}

