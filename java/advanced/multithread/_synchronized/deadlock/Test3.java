package advanced.multithread._synchronized.deadlock;

import java.util.concurrent.TimeUnit;

/**
 * ReentranLock支持以非阻塞方式tryLock获取锁，并返回是否加锁成功的结果。
 * 据此实现逻辑控制，通过破坏死锁的占有且等待这一必要条件，可以很好地解决死锁问题。

 * tryLock()
    只会尝试一次，以非公平的方式抢占锁，如果抢占成功返回true，如果加锁失败则返回false。
 * tryLock(timeout, unit)
    设置最大阻塞时间，当获取锁失败时仍然会加入队列排队并阻塞（所以会有公平和非公平策略），如果在限定时间内还没获取到锁，则返回false。
 */
public class Test3 {
    public static void main(String[] args) throws InterruptedException {
        Account account1 = new Account(1000);
        Account account2 = new Account(1000);

        MyThread myThread1 = new MyThread(account1, account2, 100);
        MyThread myThread2 = new MyThread(account2, account1, 200);
        myThread1.start();
        myThread2.start();
        myThread1.join();
        myThread2.join();
        System.out.println("账户1的余额：" + account1.getMoney() + "账户2的余额:" + account2.getMoney());
    }

    static class MyThread extends Thread {
        private final Account account1;
        private final Account account2;
        private final int num;
        private boolean isFinish;

        public MyThread(Account account1, Account account2, int num) {
            this.account1 = account1;
            this.account2 = account2;
            this.num = num;
        }

        @Override
        // 账户1转账给账户2
        public void run() {
            while (!isFinish) {
                if (account1.lock.tryLock()) {      // 非阻塞的方式获取锁不存在等待排队的情况，自然就只有nonFair的实现
                    try {
                        sleep(100);
                        if (account2.lock.tryLock()) {
                            try {
                                account1.subMoney(num);
                                account2.addMoney(num);
                                isFinish = true;
                            } finally {
                                account2.lock.unlock();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        account1.lock.unlock();
                    }
                }
            }
        }
    }

}

