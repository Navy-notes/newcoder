package advanced.multithread._synchronized.deadlock;

import java.util.concurrent.locks.ReentrantLock;

/**
 * ReentranLock和sychronized一样，也是重量级锁，阻塞后并不释放锁。所以使用ReentranLock阻塞方式获取锁，还是会有死锁问题。
 */
public class Test2 {
    public static void main(String[] args) throws InterruptedException {
        Account account1 = new Account(1000);
        Account account2 = new Account(1000);

        MyThread myThread1 = new MyThread(account1, account2, 100);
        MyThread myThread2 = new MyThread(account2, account1, 100);
        myThread1.start();
        myThread2.start();
        myThread1.join();
        myThread2.join();
        System.out.println("账户1的余额：" + account1.getMoney() + "账户2的余额:" + account2.getMoney());
    }

    static class MyThread extends Thread {
        private final Account account1;
        private final Account account2;
        private final int num;

        public MyThread(Account account1, Account account2, int num) {
            this.account1 = account1;
            this.account2 = account2;
            this.num = num;
        }

        @Override
        // 账户1转账给账户2
        public void run() {
            try {
                account1.lock.lock();
                account1.subMoney(num);
                sleep(100);        // 确保复现死锁问题
                try {
                    account2.lock.lock();
                    account2.addMoney(num);
                } finally {
                    account2.lock.unlock();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                account1.lock.unlock();
            }
        }
    }

}

