package advanced.multithread._synchronized.deadlock;

import java.util.concurrent.locks.ReentrantLock;

/**
 * @author Yuliang.Lee
 * @version 1.0
 * @date 2021/12/28 16:57
 */
public class Account {
    private int money;
    public ReentrantLock lock = new ReentrantLock();

    public Account(int money) {
        this.money = money;
    }

    public void addMoney(int num) {
        money += num;
    }


    public boolean subMoney(int num) {
        if(money >= num) {
            money -= num;
            return true;
        }
        return false;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public int getMoney() {
        return money;
    }

}
