package advanced.nio;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * 在不使用多线程的情况下，BIO Server端是无法处理并发的
 * netcat作为服务端也是用BIO实现的：nc -l -s 127.0.0.1 -p 7496
 */
public class BioServer {

    public static void main(String[] args) {
        ServerSocket serverSocket = null;
        try {
            // listener单线程
            serverSocket = new ServerSocket();
            serverSocket.bind(new InetSocketAddress(12315));
            while (true) {
                System.out.println("等待连接...");
                // 阻塞
                Socket socket = serverSocket.accept();
                System.out.println("连接成功，等待数据接收...");
                // 创建了与客户端的socket连接后，这里必须要多开一个线程new Thread(while(true){持续接收消息流}).start()
                // 阻塞，read读了多少字节
                byte[] content = new byte[1024];
                int read = socket.getInputStream().read(content);


                System.out.println("数据接收成功：" + new String(content));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
