package advanced.nio;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;


/**
 * NIO Server端单线程非阻塞式处理并发，可以解决BIO Server中过多无用线程占用资源、开销过大的问题
 */
public class SimpleNioServer {

    /**
     * 弊端：需要用户自己来维护这个list，并且要不断轮询感知是否有客户端发送数据
     * NIO代码优化：（NoBlocking IO）
         window
         应用系统-----------------操作系统的函数select()-----------------轮询

         linux
         epoll()，可以主动感知有数据的socket，不需要轮询，减少性能浪费

         redis-------gcc-----aaa.c------epoll()--------redis官网是没有window版本的
     */
    private static List<SocketChannel> list = new ArrayList<>();


    public static void main(String[] args) {
        try {
            ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
            serverSocketChannel.bind(new InetSocketAddress(12315));
            // 1、设置接收连接的对象为非阻塞，默认是阻塞的
            serverSocketChannel.configureBlocking(false);

            while (true) {
                SocketChannel socketChannel = serverSocketChannel.accept();
                if (socketChannel == null) {
                    Thread.sleep(1000);
//                    System.out.println("no connection...");
                } else {
                    // 2、设置接收数据的对象为非阻塞，默认也是阻塞的
                    socketChannel.configureBlocking(false);
                    list.add(socketChannel);
                    System.out.println("-------has connection: " + list.size() + "-----------");
                }

                // 3、不断轮询所有已建立连接的客户端，感知是否有发送数据
                for (SocketChannel channel : list) {
                    ByteBuffer byteBuffer = ByteBuffer.allocate(512);     // ByteBuffer会使用堆外内存
                    int msgLength = channel.read(byteBuffer);
                    if (msgLength > 0) {
                        byteBuffer.flip();
                        System.out.println(Charset.forName("utf-8").decode(byteBuffer).toString());
                    }
                }


            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
