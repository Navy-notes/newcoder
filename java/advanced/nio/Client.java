package advanced.nio;

import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

/**
 * 客户端线程
 */
public class Client {
    public static void main(String[] args) {
        Socket socket = null;
        try {
            socket = new Socket("localhost", 12315);
            Scanner scanner = new Scanner(System.in);
            while (true) {
                socket.getOutputStream().write(scanner.nextLine().getBytes());
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
