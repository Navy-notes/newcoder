package advanced.other;

/**
 * Spring中的Controller是单例还是多例的，如何保证并发性？
    默认是单例的，如果编程不慎，容易引发线程安全的问题。
 * 解决方法：
    从JDK层面实现线程安全：
     1.保证对象的无状态性和独立性，使用局部变量代替全局变量或者使用实例变量代替类变量，单例变多例。
     2.使用锁机制。
     3.不加锁实现线程安全: CAS-atomic、ThreadLocal、Copy-On-Write写时复制。
    从Spring层面，调节scope参数，根据作用域创建不同的实例，保证副本的独立性，实现线程安全。
 *
 */
//@RestController     默认是单例模式
//@Scope("prototype") 根据作用范围，使用多例模式
//  request
//  session
//  global session
public class SpringControllerDemo {
    // ThreaLocal或atomic类
    private int num = 0;

    //@RequestMapping("/num1")
    public String num1() {
        num += 10;
        return "ok=" + num;
    }

    //@RequestMapping("/num2")
    public String num2() {
        num += 1;
        return "ok=" + num;
    }

}
