package advanced.other;

import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.io.Serializable;

/**
 * 创建对象的三种方式：
 *   构造器---new、反射
 *   clone---浅拷贝
 *   序列化
 */
public class CreateObjectDemo {
    static class User implements Cloneable, Serializable {
        private String name;
        private int age;

        public User() {
            System.out.println("无参构造器");
        }
        public User(String name, int age) {
            this.name = name;
            this.age = age;
            System.out.println("有参构造器");
        }

        @Override
        public String toString() {
            return "User{" +
                    "name='" + name + '\'' +
                    ", age=" + age +
                    '}';
        }

        @Override
        protected Object clone() throws CloneNotSupportedException {
            System.out.println("clone User对象");
            return super.clone();
        }
    }

    public static void main(String[] args) throws Exception {
        /**
         * 一、通过构造器的方式
         */
//        User u1 = new User();                               // new 无参构造
        User u2 = new User("zhangsan", 20);       // new 有参构造
        User u3 = User.class.getConstructor().newInstance();  // 反射创建，但其实还是使用了构造器

        /**
         * 二、通过clone（浅拷贝）的方式，前提是需要实现Clonable接口并显式重写clone()方法
         *
         * 浅拷贝与深拷贝的区别： https://www.cnblogs.com/shakinghead/p/7651502.html
         *  简单的说就是，对于一个对象的Object属性，浅拷贝只会拷贝引用值而深拷贝会为引用类型的成员变量开辟新的内存空间。
         *  JDK实现的clone方法是浅拷贝的，如果要实现深拷贝可以通过
         *      1.为对象图的每一层的每一个对象都实现Cloneable接口并重写clone方法，最后在最顶层的类的重写的clone方法中调用所有的clone方法即可实现深拷贝。
         *        即，每一层的每个对象都进行浅拷贝 = 深拷贝。
         *      2.通过对象序列化和反序列化实现深拷贝。
         *        优点是大幅精简代码，缺点是如果某个属性被transient修饰，序列化的时候将不被保存，所以对象反序列化的时候该属性自然也就无法被拷贝了。
         */
        User u4 = (User) u2.clone();
        System.out.println(u4 == u2);

        /**
         * 三、通过序列化，前提是需要实现Serializable接口
         */
        File file = new File("F:\\IDEAProjects\\newcoder\\java\\resource\\user2serial.txt");
//        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file));
//        oos.writeObject(u2);
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
        Object u5 = ois.readObject();
        System.out.println("---------------");
        System.out.println(u5.toString());
        System.out.println(u5 == u2);
        System.out.println("---------------");

        UserEnum.INSTANCE.doSomething();
    }


    /**
     * 通过枚举实现绝对安全的单例
     */
    public enum UserEnum {
        INSTANCE;
        public void doSomething() {
            //xxxx
            System.out.println("单例模式...");
        }
    }
}
