package advanced.jvm;

import org.openjdk.jol.info.ClassLayout;

/**
 * 一个Java对象的内存结构：(对象头 + 实例数据 + 对齐位)
    markword                 8字节
    class pointer类型指针    一般4字节
    instance data实例数据    根据数据类型判断
    padding对齐              要求能被8整除
 */
public class PrintClassLayout {

    private static class T {
        int a;
        long b;
        boolean flag;
        String str = "hello";       // 对象类型则不保存真实的数据，而是保存引用地址(指针)
    }

    public static void main(String[] args) {
        T t = new T();
//        System.out.println(ClassLayout.parseInstance(T.class).toPrintable());
        System.out.println(ClassLayout.parseInstance(t).toPrintable());
    }
}
