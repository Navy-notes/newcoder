package advanced.jvm;

/**
 * 一个对象的创建和初始化过程：
      首先会加载这个对象的类文件，加载到内存后如果发现该类有父类，需要先跳转加载其父类并完成父类的静态初始化后再返回来完成子类的静态初始化。
          注意，静态资源指静态变量、常量、静态方法、静态代码块，静态资源都存放在JVM的方法区。
          静态初始化会且只会在Class加载的时候运行一次。类的静态资源有且只有一份，由所有对象实例共享。
      完成类及其父类(根基类)的加载及静态初始化后，会开始构造对象。构造对象分两步，一步是创建对象，然后是初始化(init)对象。
          创建对象会先在JVM堆给对象分配内存空间，然后创建实例属性和方法并设置默认值。(默认数字为 0，字符为 null，布尔为 false，而所有引用被设置成 null）
          在完成对象的创建后，准备初始化实例之前，会先检查如果该类有父类，那么还需要先构造父类的对象实例（即创建和初始化的流程递归走一遍）。
          完成父类对象的构造后，初始化自己的对象，即把实例属性设置为程序员指定的初始赋值。
          最后，执行构造方法中程序员定义的部分，然后返回对象的内存地址。

          以Dog实际的完整的构造函数为例：
             Dog() {
                 // 创建默认的属性和方法（JVM隐式）
                 // 调用父类的构造函数super()（可显式写出）
                 // 对默认属性和方法分别进行赋值和初始化、执行非静态代码块（JVM隐式）
                 // 执行程序员/用户实现的逻辑 （显式）
             }
          我们常说的对象半初始化，其实就是只完成了对象创建而还未完成初始化。

 * 参考资料：https://www.runoob.com/w3cnote/java-init-object-process.html
 */
public class InitProcessTest {
    //父类Animal
    static class Animal {
        /*8、执行初始化*/
        private int i = 9;
        protected int j;

        /*7、调用构造方法，创建默认属性和方法，完成后发现自己没有父类*/
        public Animal() {
            /*9、执行构造方法剩下的内容，结束后回到子类构造函数中*/
            System.out.println("i = " + i + ", j = " + j);
            j = 39;
        }

        /*2、初始化根基类的静态对象和静态方法*/
        private static int x1 = print("static Animal.x1 initialized");
        static int print(String s) {
            System.out.println(s);
            return 47;
        }
    }

    //子类 Dog
    static class Dog extends Animal {
        /*10、初始化默认的属性和方法*/
        private int k = print("Dog.k initialized");
        public int test = 999;

        /*6、开始创建对象，即分配存储空间->创建默认的属性和方法。
         * 遇到隐式或者显式写出的super()跳转到父类Animal的构造函数。
         * super()要写在构造函数第一行 */
        public Dog() {
            /*11、初始化结束执行剩下的用户定义的语句*/
            this.test = 888;
            System.out.println("k = " + k);
            System.out.println("j = " + j);
        }

//        {
//            System.out.println("-------------" + test + "------------------");
//        }

        /*3、初始化子类的静态对象静态方法，当然mian函数也是静态方法*/
        private static int x2 = print("static Dog.x2 initialized");

        /*1、要执行静态main，首先要加载Dog.class文件，加载过程中发现有父类Animal，
         *所以要先去加载Animal.class文件，直至找到根基类，这里就是Animal*/
        public static void main(String[] args) {

            /*4、前面步骤完成后执行main方法，输出语句*/
            System.out.println("Dog constructor");
            /*5、遇到new Dog()，调用Dog对象的构造函数*/
            Dog dog = new Dog();
            /*12、运行main函数余下的部分程序*/
            System.out.println("Main Left");
        }
    }
}
